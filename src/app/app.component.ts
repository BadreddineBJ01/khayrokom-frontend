import { Component, OnInit } from '@angular/core';
import { Platform, MenuController } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { RouterHistoryService } from './services/routerHistory.service';
import { UserService } from './services/users.service';
import { NavigationEnd, Router } from '@angular/router';
import { Storage } from '@ionic/storage';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss'],
})
export class AppComponent implements OnInit {
  connectedUserRole: any;
  public appPages = [
    {
      title: 'Home',
      url: '/home',
      icon: 'home',
    },
    {
      title: 'Institutes',
      url: '/institutes',
      icon: 'business',
    },
    {
      title: 'Halqat',
      url: '/halaqat',
      icon: 'business',
    },
    {
      title: 'Classes',
      url: '/classes',
      icon: 'business',
    },
    {
      title: 'Students',
      url: '/students',
      icon: 'person',
    },
    {
      title: 'Achivements',
      url: '/achievements',
      icon: 'list-box',
    },
    {
      title: 'Exams',
      url: '/home',
      icon: 'flash',
    },
    {
      title: 'Users',
      url: '/users',
      icon: 'contacts',
    },
    {
      title: 'Statistics',
      url: '/home',
      icon: 'trending-up',
    },
    {
      title: 'Profile',
      url: '/profile',
      icon: 'person',
    },
    {
      title: 'Logout',
      url: '/auth',
      icon: 'log-out',
    },
  ];
  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    private storage: Storage,
    private router: Router,
    private userService: UserService
  ) {
    this.initializeApp();
  }

  initializeApp() {
    this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      this.splashScreen.hide();
      this.SetUserRole();
    });
  }

  ngOnInit() {
    this.router.events.subscribe((res) => {
      if (res instanceof NavigationEnd) {
        this.SetUserRole();
      }
    });
  }

  public async get(settingName) {
    return await this.storage.get(`${settingName}`);
  }

  async SetUserRole() {
    this.connectedUserRole = await this.storage.get('ROLE');
  }

  Logout() {
    this.userService.logout();
  }
}
