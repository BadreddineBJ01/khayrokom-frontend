import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators, NgForm } from '@angular/forms';
import { Storage } from '@ionic/storage';
import { UserService } from 'src/app/services/users.service';
import { Router } from '@angular/router';
import { ToastController } from '@ionic/angular';
import { BehaviorSubject } from 'rxjs';
import { EventEmitter } from 'protractor';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {
  LoginForm: FormGroup;
  local: any;
  LoginEvent = new BehaviorSubject<boolean>(false);
  showPassword = false;
  PasswordToggleIcon = 'eye';

  error_messages = {
    email: [{ type: 'required', message: 'Email is required.' }],
    password: [{ type: 'required', message: 'Password is required.' }],
  };

  constructor(
    private formBuilder: FormBuilder,
    private storage: Storage,
    private router: Router,
    private userService: UserService,
    public toastController: ToastController
  ) {}

  ngOnInit() {
    this.initLoginform();
  }

  initLoginform() {
    this.LoginForm = this.formBuilder.group({
      email: ['', Validators.required],
      password: ['', Validators.required],
    });
  }

  loginIn(loginForm: FormGroup) {
    this.userService.loginUser(loginForm.value).subscribe(
      (res) => {
        if (res.code === '0000') {
          this.presentToast(res.message, 'success');
          this.setLocalStorage(res);
          this.router.navigate(['/home']);
        } else {
          console.log('error', res.message);
          this.presentToast(res.message, 'error');
        }
      },
      (err) => {
        this.presentToast(err, 'error');
        console.log('error', err);
      }
    );
  }

  async setLocalStorage(res: any) {
    console.log('response to set', res);
    this.storage.set('token', res.content.token);
    this.storage.set('USERINFO', res.content.user);
    this.storage.set('USERID', res.content.user.userId);
    this.storage.set('ROLE', res.content.user.role);
    this.storage.set('ISLOGGEDIN', true);

    this.storage.get('token').then((value) => {});
  }

  async presentToast(msg: any, messagetype) {
    const toast = await this.toastController.create({
      message: msg,
      duration: 3000,
      position: 'bottom',
      color: messagetype === 'success' ? 'success' : 'danger',
    });
    toast.present();
  }

  togglePassword(): void {
    this.showPassword = !this.showPassword;

    if (this.PasswordToggleIcon == 'eye') {
      this.PasswordToggleIcon = 'eye-off';
    } else {
      this.PasswordToggleIcon = 'eye';
    }
  }
}
