import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators, NgForm } from '@angular/forms';
import { UserService } from 'src/app/services/users.service';
import { ToastController } from '@ionic/angular';
import { Router } from '@angular/router';

@Component({
  selector: 'app-register',
  templateUrl: './register.page.html',
  styleUrls: ['./register.page.scss'],
})
export class RegisterPage implements OnInit {
  registerForm: FormGroup;
  // tslint:disable-next-line: variable-name
  error_messages = {
    firstname: [{ type: 'required', message: 'First Name is required.' }],

    lastname: [{ type: 'required', message: 'Last Name is required.' }],

    email: [
      { type: 'required', message: 'Email is required.' },
      { type: 'minlength', message: 'Email length.' },
      { type: 'maxlength', message: 'Email length.' },
      { type: 'required', message: 'please enter a valid email address.' },
    ],

    password: [
      { type: 'required', message: 'password is required.' },
      { type: 'minlength', message: 'password length.' },
      { type: 'maxlength', message: 'password length.' },
    ],
    confirmpassword: [
      { type: 'required', message: 'password is required.' },
      { type: 'minlength', message: 'password length.' },
      { type: 'maxlength', message: 'password length.' },
    ],
  };
  showPassword = false;
  PasswordToggleIcon = 'eye';

  constructor(
    private formBuilder: FormBuilder,
    private userService: UserService,
    private toastController: ToastController,
    private router: Router
  ) {}

  ngOnInit() {
    this.registerForm = this.InitRegisterForm();
  }

  InitRegisterForm() {
    return this.formBuilder.group(
      {
        firstname: ['', Validators.compose([Validators.required])],
        lastname: ['', Validators.compose([Validators.required])],
        email: [
          '',
          Validators.compose([
            Validators.required,
            Validators.minLength(6),
            Validators.maxLength(100),
            Validators.pattern(
              '^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+.[a-zA-Z0-9-.]+$'
            ),
          ]),
        ],
        phone: [''],
        password: [
          '',
          Validators.compose([
            Validators.required,
            Validators.minLength(6),
            Validators.maxLength(30),
          ]),
        ],
        confirmpassword: [
          '',
          Validators.compose([
            Validators.required,
            Validators.minLength(6),
            Validators.maxLength(30),
          ]),
        ],
      },
      {
        validators: this.password.bind(this),
      }
    );
  }

  password(formGroup: FormGroup) {
    const { value: password } = formGroup.get('password');
    const { value: confirmPassword } = formGroup.get('confirmpassword');
    return password === confirmPassword ? null : { passwordNotMatch: true };
  }

  registerUser(UserToAdd: NgForm) {
    this.userService.registerUser(UserToAdd).subscribe(
      (res) => {
        if (res.code === '0000') {
          this.presentToast(res.message, 'success');
          this.resetForm();
          this.router.navigate(['/auth']);
        } else {
          this.presentToast(res.message, 'error');
        }
      },
      (err) => {
        this.presentToast(err, 'error');
      }
    );
  }

  async presentToast(msg: any, messagetype) {
    const toast = await this.toastController.create({
      message: msg,
      duration: 3000,
      position: 'bottom',
      color: messagetype === 'success' ? 'success' : 'danger',
    });
    toast.present();
  }

  resetForm() {
    this.registerForm.reset();
  }

  togglePassword(): void {
    this.showPassword = !this.showPassword;

    if (this.PasswordToggleIcon == 'eye') {
      this.PasswordToggleIcon = 'eye-off';
    } else {
      this.PasswordToggleIcon = 'eye';
    }
  }
}
