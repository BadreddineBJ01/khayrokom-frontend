import {sharedModel} from '../../sharedModels/sharedModel';

export interface UserCredentials {
    email: string;
    password: string;
}


export class UserModel extends sharedModel {
    firstname: string;
	lastname: string;
	email: string;
	phone: string;

    constructor(user?: any) {
        user = user || {};
        super(user);
        this.firstname = this.firstname || '';
        this.lastname = this.lastname || '';
        this.email = this.email || '';
        this.phone = this.phone || '';
     }
}

