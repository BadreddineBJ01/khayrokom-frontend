import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ToastController } from '@ionic/angular';
import { UserService } from 'src/app/services/users.service';

@Component({
  selector: 'app-reset-password',
  templateUrl: './reset-password.page.html',
  styleUrls: ['./reset-password.page.scss'],
})
export class ResetPasswordPage implements OnInit {
  EmailForm: FormGroup;
  error_messages = {
    email: [
      { type: 'required', message: 'Email is required.' },
      { type: 'minlength', message: 'Email length.' },
      { type: 'maxlength', message: 'Email length.' },
      { type: 'required', message: 'please enter a valid email address.' },
    ],
  };

  constructor(
    private _fb: FormBuilder,
    private usersService: UserService,
    public toastController: ToastController,
    private router: Router
  ) {}

  ngOnInit() {
    this.initResetform();
  }

  initResetform() {
    this.EmailForm = this._fb.group({
      email: [
        '',
        Validators.compose([
          Validators.required,
          Validators.minLength(6),
          Validators.maxLength(100),
          Validators.pattern('^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+.[a-zA-Z0-9-.]+$'),
        ]),
      ],
    });
  }

  sendResetEmail() {
    this.usersService.requestReset(this.EmailForm.value).subscribe(
      (data) => {
        this.EmailForm.reset();
        const successMessage =
          'Reset password link send to email sucessfully, Please Check Your Email';
        this.presentToast(successMessage, 'success');
        setTimeout(() => {
          this.router.navigate(['/auth/login']);
        }, 3000);
      },
      (err) => {
        if (err.error.message) {
          const errorMessage = err.error.message;
          this.presentToast(errorMessage, 'error');
        }
      }
    );
  }

  async presentToast(msg: any, messagetype) {
    const toast = await this.toastController.create({
      message: msg,
      duration: 3000,
      position: 'bottom',
      color: messagetype === 'success' ? 'success' : 'danger',
    });
    toast.present();
  }
}
