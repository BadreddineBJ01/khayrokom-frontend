import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';
import { ResetPasswordPageRoutingModule } from './reset-password-routing.module';
import { ResetPasswordPage } from './reset-password.page';
import { SharedModuleModule } from 'src/app/shared-module/shared-module.module';
import { ResponseResetComponentComponent } from './response-reset-component/response-reset-component.component';

@NgModule({
  imports: [
    CommonModule,
    IonicModule,
    SharedModuleModule,
    ResetPasswordPageRoutingModule,
  ],
  declarations: [ResetPasswordPage, ResponseResetComponentComponent],
})
export class ResetPasswordPageModule {}
