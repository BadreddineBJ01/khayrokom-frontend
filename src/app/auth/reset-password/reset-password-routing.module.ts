import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ResetPasswordPage } from './reset-password.page';
import { ResponseResetComponentComponent } from './response-reset-component/response-reset-component.component';

const routes: Routes = [
  {
    path: 'emailnotif',
    component: ResetPasswordPage,
  },
  {
    path: 'resetpassword/:token',
    component: ResponseResetComponentComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ResetPasswordPageRoutingModule {}
