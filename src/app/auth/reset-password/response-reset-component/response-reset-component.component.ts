import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastController } from '@ionic/angular';
import { UserService } from 'src/app/services/users.service';

@Component({
  selector: 'app-response-reset-component',
  templateUrl: './response-reset-component.component.html',
  styleUrls: ['./response-reset-component.component.scss'],
})
export class ResponseResetComponentComponent implements OnInit {
  resetToken: any;
  ResponseResetForm: FormGroup;
  CurrentState: any;
  showPassword = false;
  PasswordToggleIcon = 'eye';

  error_messages = {
    newPassword: [
      { type: 'required', message: 'password is required.' },
      { type: 'minlength', message: 'password length.' },
      { type: 'maxlength', message: 'password length.' },
    ],
    confirmPassword: [
      { type: 'required', message: 'password is required.' },
      { type: 'minlength', message: 'password length.' },
      { type: 'maxlength', message: 'password length.' },
    ],
  };

  constructor(
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private userservice: UserService,
    private fb: FormBuilder,
    private toastController: ToastController
  ) {
    this.CurrentState = 'Wait';
    this.activatedRoute.params.subscribe((params: any) => {
      if (params !== {} && params.token !== undefined) {
        this.resetToken = params.token;
        this.VerifyToken();
      }
    });
  }

  ngOnInit() {
    this.Init();
  }

  VerifyToken() {
    this.userservice
      .ValidPasswordToken({ resettoken: this.resetToken })
      .subscribe(
        (data) => {
          this.CurrentState = 'Verified';
        },
        (err) => {
          this.CurrentState = 'NotVerified';
        }
      );
  }

  Init() {
    this.ResponseResetForm = this.fb.group(
      {
        resettoken: [this.resetToken],
        newPassword: [
          '',
          Validators.compose([
            Validators.required,
            Validators.minLength(6),
            Validators.maxLength(30),
          ]),
        ],
        confirmPassword: [
          '',
          Validators.compose([
            Validators.required,
            Validators.minLength(6),
            Validators.maxLength(30),
          ]),
        ],
      },
      {
        validators: this.password.bind(this),
      }
    );
  }

  password(formGroup: FormGroup) {
    const { value: password } = formGroup.get('newPassword');
    const { value: confirmPassword } = formGroup.get('confirmPassword');
    return password === confirmPassword ? null : { passwordNotMatch: true };
  }

  Validate(passwordFormGroup: FormGroup) {
    const new_password = passwordFormGroup.controls.newPassword.value;
    const confirm_password = passwordFormGroup.controls.confirmPassword.value;

    if (confirm_password.length <= 0) {
      return null;
    }

    if (confirm_password !== new_password) {
      return {
        doesNotMatch: true,
      };
    }

    return null;
  }

  ResetPassword(form) {
    console.log(form.get('confirmPassword'));
    this.userservice.newPassword(this.ResponseResetForm.value).subscribe(
      (data) => {
        this.ResponseResetForm.reset();
        let successMessage = data.message;
        this.presentToast(successMessage, 'success');
        setTimeout(() => {
          this.router.navigate(['/auth/login']);
        }, 3000);
      },
      (err) => {
        if (err.error.message) {
          let errorMessage = err.error.message;
          this.presentToast(errorMessage, 'error');
        }
      }
    );
  }

  async presentToast(msg: any, messagetype) {
    const toast = await this.toastController.create({
      message: msg,
      duration: 5000,
      position: 'bottom',
      color: messagetype === 'success' ? 'success' : 'danger',
    });
    toast.present();
  }

  togglePassword(): void {
    this.showPassword = !this.showPassword;

    if (this.PasswordToggleIcon == 'eye') {
      this.PasswordToggleIcon = 'eye-off';
    } else {
      this.PasswordToggleIcon = 'eye';
    }
  }
}
