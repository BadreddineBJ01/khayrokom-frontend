import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators, NgForm } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { UserService } from 'src/app/services/users.service';
import { UsersModel } from '../models/users.models';
import { ToastController } from '@ionic/angular';
import { InstitutService } from 'src/app/services/institut.service';

@Component({
  selector: 'app-add-user',
  templateUrl: './add-user.page.html',
  styleUrls: ['./add-user.page.scss'],
})
export class AddUserPage implements OnInit {
  UsersForm: FormGroup;
  UserToEdit = new UsersModel();
  EditUserInfo = false;
  userId: any;
  listInstitutes: any[] = [];
  connectedUserRole: any;
  showPassword = false;
  showConfirmPassword = false;
  PasswordToggleIcon = 'eye';
  ConfirmPasswordToggleIcon = 'eye';

  // Login Forms
  error_messages = {
    firstname: [{ type: 'required', message: 'First Name is required.' }],

    lastname: [{ type: 'required', message: 'Last Name is required.' }],

    email: [
      { type: 'required', message: 'Email is required.' },
      { type: 'minlength', message: 'Email length.' },
      { type: 'maxlength', message: 'Email length.' },
      { type: 'required', message: 'please enter a valid email address.' },
    ],

    password: [
      { type: 'required', message: 'password is required.' },
      { type: 'minlength', message: 'password length.' },
      { type: 'maxlength', message: 'password length.' },
    ],
    confirmpassword: [
      { type: 'required', message: 'password is required.' },
      { type: 'minlength', message: 'password length.' },
      { type: 'maxlength', message: 'password length.' },
    ],
  };

  constructor(
    public formBuilder: FormBuilder,
    public router: Router,
    public userService: UserService,
    public institutService: InstitutService,
    public toastController: ToastController,
    private activatedRoute: ActivatedRoute
  ) {}

  ngOnInit() {
    this.getAllInstitutes();
    this.initForm();
    this.activatedRoute.params.subscribe((params: any) => {
      console.log('params', params);
      if (params !== {} && params.id !== undefined) {
        this.GetUserById(params.id);
        this.userId = params.id;
        this.EditUserInfo = true;
      } else {
        this.EditUserInfo = false;
        this.initForm();
      }
    });
    this.getConnectedUserRole();
  }

  async getConnectedUserRole() {
    this.connectedUserRole = await this.userService.getUserRole();
  }

  getAllInstitutes() {
    this.institutService.getAllInstitutes().subscribe(
      (data) => {
        if (data.code === '0000') {
          console.log('data', data);
          this.listInstitutes = data.content;
        } else {
          console.log('error');
        }
      },
      (err) => {
        console.log('subscription error', err);
      }
    );
  }

  GetUserById(userId: any) {
    console.log('params --->', userId);
    this.userService.getUserById(userId).subscribe(
      (data) => {
        if (data.code === '0000') {
          console.log('data content', data.content[0]);
          this.UserToEdit = new UsersModel(data.content[0]);
          console.log('this.InstitutToEdit', this.UserToEdit);
          this.initForm();
        } else {
          console.log('error', data);
        }
      },
      (err) => {
        console.log('error', err);
      }
    );
  }

  initForm() {
    this.UsersForm = this.formBuilder.group(
      {
        firstname: [
          this.UserToEdit.firstname,
          Validators.compose([Validators.required]),
        ],
        lastname: [
          this.UserToEdit.lastname,
          Validators.compose([Validators.required]),
        ],
        email: [
          this.UserToEdit.email,
          Validators.compose([
            Validators.required,
            Validators.minLength(6),
            Validators.maxLength(30),
            Validators.pattern(
              '^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+.[a-zA-Z0-9-.]+$'
            ),
          ]),
        ],
        phone: [this.UserToEdit.phone],
        role: [this.UserToEdit.role],
        institutesids: [this.UserToEdit.institutesids],
        password: [
          '',
          Validators.compose([
            Validators.required,
            Validators.minLength(6),
            Validators.maxLength(30),
          ]),
        ],
        confirmpassword: [
          '',
          Validators.compose([
            Validators.required,
            Validators.minLength(6),
            Validators.maxLength(30),
          ]),
        ],
      },
      {
        validators: this.password.bind(this),
      }
    );
  }

  password(formGroup: FormGroup) {
    const { value: password } = formGroup.get('password');
    const { value: confirmPassword } = formGroup.get('confirmpassword');
    return password === confirmPassword ? null : { passwordNotMatch: true };
  }

  addUser(UsersForm: FormGroup) {
    console.log('ngForm', UsersForm.value);
    const usertoSend = Object.assign({}, UsersForm.value);
    delete usertoSend.confirmpassword;
    console.log('usertosend', usertoSend);

    this.userService.addUser(UsersForm.value).subscribe(
      (data) => {
        if (data.code === '0000') {
          this.presentToast(data.message, 'success');
          console.log('user added with success');
          this.GoBackToList();
        } else {
          this.presentToast(data.message, 'error');
          console.log('error');
        }
      },
      (err) => {
        this.presentToast(err, 'error');
        console.log('error', err);
      }
    );
  }

  UpdateUser(UsersForm: FormGroup) {
    console.log('users.value', UsersForm.value, this.userId);

    const usertoUpdate = Object.assign({}, UsersForm.value);
    delete usertoUpdate.confirmpassword;
    delete usertoUpdate.password;
    console.log('usertoUpdate', usertoUpdate);

    this.userService.updateUser(this.userId, usertoUpdate).subscribe(
      (data) => {
        if (data.code === '0000') {
          this.presentToast(data.message, 'success');
          console.log('user updated with success');
          this.GoBackToList();
        } else {
          this.presentToast(data.message, 'error');
          console.log('error update');
        }
      },
      (err) => {
        this.presentToast(err, 'error');
        console.log('error', err);
      }
    );
  }

  async presentToast(msg: any, messagetype) {
    const toast = await this.toastController.create({
      message: msg,
      duration: 3000,
      position: 'bottom',
      color: messagetype === 'success' ? 'success' : 'danger',
    });
    toast.present();
  }

  GoBackToList() {
    this.router.navigate(['/users/users-list']);
  }

  togglePassword(): void {
    this.showPassword = !this.showPassword;

    if (this.PasswordToggleIcon == 'eye') {
      this.PasswordToggleIcon = 'eye-off';
    } else {
      this.PasswordToggleIcon = 'eye';
    }
  }
}
