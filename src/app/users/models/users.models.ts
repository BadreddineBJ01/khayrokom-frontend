import {sharedModel} from '../../sharedModels/sharedModel';


export interface User {
    firstname: string;
    lastname: string;
    email: string;
    phone: string;
    password: string;
    role: string;
    institutesids: any[];
}

export class UsersModel extends sharedModel {
    firstname: string;
    lastname: string;
    email: string;
    phone: string;
    role: string;
    institutesids: any[];

    constructor(user?: any) {
         user = user || {};
         super(user);
         this.firstname = user.firstname || '';
         this.lastname = user.lastname || '';
         this.email = user.email || '';
         this.phone = user.phone || '';
         this.role = user.role || '';
         this.institutesids = user.institutesids || []; 
    }
}
