import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { UserService } from 'src/app/services/users.service';

@Component({
  selector: 'app-user-details',
  templateUrl: './user-details.page.html',
  styleUrls: ['./user-details.page.scss'],
})
export class UserDetailsPage implements OnInit {
  userId: any;
  userDetails: any;
  constructor(private activatedRoute: ActivatedRoute,
              private userService: UserService) { }

  ngOnInit() {
    this.userId = this.activatedRoute.snapshot.paramMap.get('id');
    this.getUserById(this.userId);
  }

  getUserById(userId: any) {
    this.userService.getUserById(userId).subscribe(data => {
     if (data.code === '0000') {
       console.log(data.content);
       this.userDetails = data.content[0];
     } else {
      console.log('error', data);
     }
    }, (err) => {
      console.log('error', err);
    });
   }
}
