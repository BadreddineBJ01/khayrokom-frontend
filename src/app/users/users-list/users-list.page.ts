import { Component, OnInit } from '@angular/core';
import { UserService } from 'src/app/services/users.service';
import { Router, NavigationEnd } from '@angular/router';
import {
  AlertController,
  ModalController,
  LoadingController,
  ToastController,
} from '@ionic/angular';

@Component({
  selector: 'app-users-list',
  templateUrl: './users-list.page.html',
  styleUrls: ['./users-list.page.scss'],
})
export class UsersListPage implements OnInit {
  // adapt to the user model types
  usersList: any[] = [];
  constructor(
    private userService: UserService,
    private router: Router,
    private alertCtrl: AlertController,
    public modalController: ModalController,
    public toastController: ToastController,
    private loadingController: LoadingController
  ) {}

  ngOnInit() {
    this.router.events.subscribe((res) => {
      if (res instanceof NavigationEnd) {
        this.getAllUsers();
      }
    });
  }

  async getAllUsers() {
    const loading = await this.loadingController.create({
      message: 'Loading...',
    });
    await loading.present();

    this.userService.getAllUsers().subscribe(
      (data) => {
        if (data.code === '0000') {
          this.usersList = data.content;
          loading.dismiss();
        } else {
          loading.dismiss();
        }
      },
      (err) => {
        loading.dismiss();
      }
    );
  }

  editUser(userId: any) {
    this.router.navigate(['users/edit-user/' + userId]);
  }

  delete(userId: any) {
    this.userService.deleteUser(userId).subscribe(
      (data) => {
        if (data.code === '0000') {
          this.presentToast(data.message, 'success');
        } else {
          this.presentToast(data.message, 'error');
          console.log('error');
        }
      },
      (err) => {
        this.presentToast(err, 'error');
        console.log('error');
      }
    );
  }

  userdetails(userId: any) {
    this.router.navigate(['users/user-details/' + userId]);
  }

  async presentConfirmDelete(userId: any) {
    const alert = this.alertCtrl.create({
      message: 'Do you want to delet this user',
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          handler: () => {
            console.log('Cancel clicked');
          },
        },
        {
          text: 'Confirm',
          handler: () => {
            this.delete(userId);
            this.ReloadList();
          },
        },
      ],
    });
    (await alert).present();
  }

  ReloadList() {
    this.getAllUsers();
  }

  async presentToast(msg: any, messagetype) {
    const toast = await this.toastController.create({
      message: msg,
      duration: 3000,
      position: 'bottom',
      color: messagetype === 'success' ? 'success' : 'danger',
    });
    toast.present();
  }
}
