import {sharedModel} from '../../sharedModels/sharedModel';

export interface Achivement {
  program: String;
  surah: String;
  fromVerse: Number;
  toVerse: Number;
  evaluation: String; // change to enum if needed
  notes: String;
  achievDate: Date;
  idstudent: string;
  created_at: Date;
}

export class AchivementModel extends sharedModel {
  program: String;
  surah: String;
  fromVerse: Number;
  toVerse: Number;
  evaluation: String; // change to enum if needed
  notes: String;
  achievDate: Date;
  idstudent: String;
  created_at: Date;

  constructor(AchivementEntity?: any) {
    AchivementEntity = AchivementEntity || {};
    super(AchivementEntity);
    this.program = AchivementEntity.program || '';
    this.surah = AchivementEntity.surah || '';
    this.fromVerse = AchivementEntity.fromVerse || ''; 
    this.toVerse = AchivementEntity.toVerse || ''; 
    this.evaluation = AchivementEntity.evaluation || ''; 
    this.notes = AchivementEntity.notes || ''; 
    this.achievDate = AchivementEntity.achievDate || '';
    this.idstudent = AchivementEntity.idstudent || '';
    this.created_at = AchivementEntity.created_at || '';
  }

}
