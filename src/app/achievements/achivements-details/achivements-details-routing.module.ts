import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AchivementsDetailsPage } from './achivements-details.page';

const routes: Routes = [
  {
    path: '',
    component: AchivementsDetailsPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AchivementsDetailsPageRoutingModule {}
