import { Component, OnInit } from '@angular/core';
import { AchivementService } from 'src/app/services/achivements.service';
import { ActivatedRoute } from '@angular/router';
import { AchivementModel } from '../models/Achivement.model';
import { surats } from '../models/QuranSurat';

@Component({
  selector: 'app-achivements-details',
  templateUrl: './achivements-details.page.html',
  styleUrls: ['./achivements-details.page.scss'],
})
export class AchivementsDetailsPage implements OnInit {
  AchivementsId: any;
  achivementsDetails: any;
  suratArray = surats;
  constructor(
    private activatedRoute: ActivatedRoute,
    private achivementService: AchivementService
  ) {}

  ngOnInit() {
    this.AchivementsId = this.activatedRoute.snapshot.paramMap.get('id');
    this.getAchivementsById(this.AchivementsId);
  }
  getAchivementsById(AchivementsId: any) {
    this.achivementService.getAchivementById(AchivementsId).subscribe(
      (data) => {
        if (data.code === '0000') {
          console.log('echiv', data.content);
          this.achivementsDetails = data.content[0];
        } else {
          console.log('error', data);
        }
      },
      (err) => {
        console.log('error', err);
      }
    );
  }
}
