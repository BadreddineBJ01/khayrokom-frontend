import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { AddAchivementPage } from './add-achivement.page';

describe('AddAchivementPage', () => {
  let component: AddAchivementPage;
  let fixture: ComponentFixture<AddAchivementPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddAchivementPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(AddAchivementPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
