import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators, NgForm } from '@angular/forms';
import { AchivementService } from 'src/app/services/achivements.service';
import { Router, ActivatedRoute, NavigationEnd } from '@angular/router';
import { AchivementModel } from '../models/Achivement.model';
import { switchMap, map } from 'rxjs/operators';
import { CommunicationService } from 'src/app/services/communication.service';
import { surats } from '../models/QuranSurat';
import { StudentsService } from 'src/app/services/Students.service';

@Component({
  selector: 'app-add-achivement',
  templateUrl: './add-achivement.page.html',
  styleUrls: ['./add-achivement.page.scss'],
})
export class AddAchivementPage implements OnInit {
  AchivementForm: FormGroup;
  AchivementToEdit = new AchivementModel();
  EditAchivementInfo = false;
  achivementId: any;
  StudentId: any;
  previousRoute: any;
  suratsArray: any[] = [];
  studentInfo: any;

  constructor(
    public formBuilder: FormBuilder,
    public router: Router,
    public achivementService: AchivementService,
    private activatedRoute: ActivatedRoute,
    private communicationService: CommunicationService,
    private studentsService: StudentsService
  ) {}

  ngOnInit() {
    this.loadAllSurat();
    this.StudentId = this.communicationService.StudentId;
    //  this.getStudentById();
    this.initForm();
    this.router.events
      .pipe(
        switchMap((navigation) =>
          this.activatedRoute.params.pipe(
            map((params) => ({ navigation, params }))
          )
        )
      )
      .subscribe((result) => {
        if (result.navigation instanceof NavigationEnd) {
          const urlfragment = result.navigation.url.split('/')[2];
          if (
            urlfragment.toLowerCase().includes('add') &&
            result.params.id !== undefined &&
            result.params !== {}
          ) {
            this.StudentId = this.communicationService.StudentId =
              result.params.id;
            this.getStudentById();
            this.EditAchivementInfo = false;
            this.initForm();
          } else if (
            urlfragment.toLowerCase().includes('edit') &&
            result.params.id !== undefined &&
            result.params !== {}
          ) {
            this.GetAchivementById(result.params.id);
            this.achivementId = result.params.id;
            this.EditAchivementInfo = true;
          } else {
            console.log('error id undefined');
          }
        }
      });
  }

  getStudentById() {
    this.studentsService.getStudentById(this.StudentId).subscribe(
      (res) => {
        if (res.code == '0000') {
          this.studentInfo = res.content[0];
        } else {
          console.log('error');
        }
      },
      (err) => {
        console.log('Error');
      }
    );
  }

  loadAllSurat() {
    this.suratsArray = surats;
  }

  GetAchivementById(AchivementId: any) {
    console.log('params --->', AchivementId);
    this.achivementService.getAchivementById(AchivementId).subscribe(
      (data) => {
        if (data.code === '0000') {
          console.log('data content', data.content[0]);
          this.AchivementToEdit = new AchivementModel(data.content[0]);
          console.log('this.AchivementToEdit', this.AchivementToEdit);
          this.initForm();
        } else {
          console.log('error', data);
        }
      },
      (err) => {
        console.log('error', err);
      }
    );
  }

  initForm() {
    this.AchivementForm = this.formBuilder.group({
      program: [this.AchivementToEdit.program, Validators.required],
      surah: [this.AchivementToEdit.surah],
      fromVerse: [this.AchivementToEdit.fromVerse],
      toVerse: [this.AchivementToEdit.toVerse],
      evaluation: [this.AchivementToEdit.evaluation],
      notes: [this.AchivementToEdit.notes],
      achievDate: [this.AchivementToEdit.achievDate],
    });
  }

  addAchivement(AchivementForm: FormGroup) {
    const achivementToAdd = {
      program: '',
      surah: '',
      fromVerse: '',
      toVerse: '',
      evaluation: '',
      notes: '',
      achievDate: '',
      idstudent: '',
    };

    Object.assign(achivementToAdd, AchivementForm.value);
    achivementToAdd.idstudent = this.StudentId;

    console.log('ngForm', achivementToAdd);

    this.achivementService.addAchivement(achivementToAdd).subscribe(
      (data) => {
        if (data.code === '0000') {
          console.log('achivement added with success');
          this.GoBackToList();
        } else {
          console.log('error');
        }
      },
      (err) => {
        console.log('error', err);
      }
    );
  }

  UpdateAchivement(AchivementForm: FormGroup) {
    const achivementToAdd = {
      program: '',
      surah: '',
      fromVerse: '',
      toVerse: '',
      evaluation: '',
      notes: '',
      achievDate: '',
      idstudent: '',
    };

    Object.assign(achivementToAdd, AchivementForm.value);
    achivementToAdd.idstudent = this.StudentId;
    this.achivementService
      .updateAchivement(this.achivementId, achivementToAdd)
      .subscribe(
        (data) => {
          if (data.code === '0000') {
            console.log('achivement updated with success');
            this.GoBackToList();
          } else {
            console.log('error update');
          }
        },
        (err) => {
          console.log('error', err);
        }
      );
  }

  GoBackToList() {
    this.router.navigate(['/achievements/achivement-list/' + this.StudentId]);
  }
}
