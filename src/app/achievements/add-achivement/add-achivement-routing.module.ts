import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AddAchivementPage } from './add-achivement.page';

const routes: Routes = [
  {
    path: '',
    component: AddAchivementPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AddAchivementPageRoutingModule {}
