import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { AddAchivementPageRoutingModule } from './add-achivement-routing.module';

import { AddAchivementPage } from './add-achivement.page';

import { SharedModuleModule } from 'src/app/shared-module/shared-module.module';

@NgModule({
  imports: [
    CommonModule,
	SharedModuleModule,
    FormsModule,
    IonicModule,
    AddAchivementPageRoutingModule
  ],
  declarations: [AddAchivementPage]
})
export class AddAchivementPageModule {}
