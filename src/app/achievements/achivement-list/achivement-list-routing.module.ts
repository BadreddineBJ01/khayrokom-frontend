import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AchivementListPage } from './achivement-list.page';
import { AchivementsDetailsPageModule } from '../achivements-details/achivements-details.module';

const routes: Routes = [
  {
    path: '',
    component: AchivementListPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AchivementListPageRoutingModule {}
