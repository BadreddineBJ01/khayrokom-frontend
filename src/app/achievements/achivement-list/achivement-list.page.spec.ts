import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { AchivementListPage } from './achivement-list.page';

describe('AchivementListPage', () => {
  let component: AchivementListPage;
  let fixture: ComponentFixture<AchivementListPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AchivementListPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(AchivementListPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
