import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { AchivementListPageRoutingModule } from './achivement-list-routing.module';

import { AchivementListPage } from './achivement-list.page';
import { AchivementsDetailsPage } from '../achivements-details/achivements-details.page';
import { AchivementsDetailsPageModule } from '../achivements-details/achivements-details.module';
import { SharedModuleModule } from 'src/app/shared-module/shared-module.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    SharedModuleModule,
    IonicModule,
    AchivementListPageRoutingModule
  ],
  declarations: [AchivementListPage]
})
export class AchivementListPageModule {}
