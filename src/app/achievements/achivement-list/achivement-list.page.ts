import { Component, OnInit } from '@angular/core';
import { AchivementService } from 'src/app/services/achivements.service';
import { Router, ActivatedRoute, NavigationEnd } from '@angular/router';
import {
  AlertController,
  ModalController,
  LoadingController,
} from '@ionic/angular';
import { AchivementsDetailsPage } from '../achivements-details/achivements-details.page';
import { CommunicationService } from 'src/app/services/communication.service';
import { UserService } from 'src/app/services/users.service';
import { FormControl, FormGroup, FormBuilder } from '@angular/forms';
import { SearchService } from 'src/app/services/search.service';
import { StudentsService } from 'src/app/services/Students.service';

@Component({
  selector: 'app-achivement-list',
  templateUrl: './achivement-list.page.html',
  styleUrls: ['./achivement-list.page.scss'],
})
export class AchivementListPage implements OnInit {
  StudentId: any;
  achivementsList: any;
  connectedUserRole: any;
  FilterForm: FormGroup;
  studentInfo: any;
  constructor(
    private achivementService: AchivementService,
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private alertCtrl: AlertController,
    public modalController: ModalController,
    private loadingController: LoadingController,
    private userService: UserService,
    private fb: FormBuilder,
    private SearchService: SearchService,
    private studentsService: StudentsService,
    private CommunicationService: CommunicationService
  ) {}

  ngOnInit() {
    // get the student id from the url
    this.activatedRoute.params.subscribe((params: any) => {
      if (params !== {} && params.id !== undefined) {
        this.StudentId = this.CommunicationService.StudentId = params.id;
        this.getStudentById();
      }
    });

    this.router.events.subscribe((res) => {
      if (res instanceof NavigationEnd) {
        this.getAllStudentAchivements();
      }
    });
    this.setUserRole();
    this.initForm();
  }

  getStudentById() {
    this.studentsService.getStudentById(this.StudentId).subscribe(
      (res) => {
        if (res.code == '0000') {
          this.studentInfo = res.content[0];
        } else {
        }
      },
      (err) => {}
    );
  }

  initForm() {
    this.FilterForm = this.fb.group({
      program: [],
    });
  }

  async setUserRole() {
    this.connectedUserRole = await this.userService.getUserRole();
  }

  SelectProgram(event: any) {
    let keyWord = event.detail.value;
    this.SearchService.StudentProgramFilter(this.StudentId, keyWord).subscribe(
      (res: any) => {
        if (res.code === '0000') {
          this.achivementsList = res.content;
        } else {
          console.log('no data found');
        }
      },
      (err) => {}
    );
  }

  async getAllStudentAchivements() {
    const loading = await this.loadingController.create({
      message: 'Loading...',
    });
    await loading.present();

    this.SearchService.StudentProgramFilter(this.StudentId, '').subscribe(
      (res: any) => {
        if (res.code === '0000') {
          this.achivementsList = res.content;
          loading.dismiss();
        } else {
          loading.dismiss();
        }
      },
      (err) => {
        loading.dismiss();
      }
    );

    // this.achivementService.getAllStudentAchivements(this.StudentId).subscribe(
    //   data => {
    //     if (data.code === '0000') {
    //       console.log('data', data);
    //       this.achivementsList = data.content;
    //       loading.dismiss();
    //     } else {
    //       console.log('error');
    //       loading.dismiss();
    //     }
    //   },
    //   err => {
    //     console.log('subscription error', err);
    //     loading.dismiss();
    //   }
    // );
  }

  // getColor(program)
  // {
  // switch(program)
  // {
  //   case 'Memorize':
  //     return 'primary';
  //     break;
  //   case 'Revision':
  //     return 'secondary';
  //   break;
  //   case 'Exam':
  //     return 'tertiary';
  //   break;
  // }
  // }

  goToAddAchivement() {
    this.router.navigate(['/achievements/add-achivement/' + this.StudentId]);
  }

  editAchivement(achivementId: any) {
    this.router.navigate(['/achievements/edit-achivement/' + achivementId]);
  }

  delete(achivementId: any) {
    this.achivementService.deleteAchivement(achivementId).subscribe(
      (data) => {
        if (data.code === '0000') {
        } else {
          console.log('error');
        }
      },
      (err) => {
        console.log('error');
      }
    );
  }

  Achivementdetails(achivementId: any) {
    this.router.navigate(['achievements/achivements-details/' + achivementId]);
  }

  async presentConfirmDelete(achivementId: any) {
    const alert = this.alertCtrl.create({
      message: 'Do you want to delet this achivement',
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          handler: () => {
            console.log('Cancel clicked');
          },
        },
        {
          text: 'Confirm',
          handler: () => {
            this.delete(achivementId);
            this.ReloadList();
          },
        },
      ],
    });
    (await alert).present();
  }

  ReloadList() {
    this.getAllStudentAchivements();
  }
}
