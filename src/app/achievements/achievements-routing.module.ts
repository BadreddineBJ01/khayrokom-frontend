import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AchievementsPage } from './achievements.page';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'achivement-list',
    pathMatch: 'full'
  },
  {
    path: 'add-achivement/:id',
    loadChildren: () => import('./add-achivement/add-achivement.module').then( m => m.AddAchivementPageModule)
  },
  {
    path: 'achivement-list/:id',
    loadChildren: () => import('./achivement-list/achivement-list.module').then( m => m.AchivementListPageModule)
  },
  {
    path: 'achivements-details/:id',
    loadChildren: () => import('./achivements-details/achivements-details.module').then( m => m.AchivementsDetailsPageModule)
  },
  {
    path: 'edit-achivement/:id',
    loadChildren: () => import('./add-achivement/add-achivement.module').then( m => m.AddAchivementPageModule)
  },
  {
    path: '**',
    redirectTo: 'achivement-list'
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AchievementsPageRoutingModule {}
