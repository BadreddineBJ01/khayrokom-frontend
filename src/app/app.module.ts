import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';
import { HTTP_INTERCEPTORS, HttpClientModule } from '@angular/common/http';
import { IonicModule, IonicRouteStrategy } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { InstitutService } from './services/institut.service';
import { HalaqaService } from './services/halaqa.service';
import { ClassesService } from './services/classes.service';
import { AchivementService } from './services/achivements.service';
import { StudentsService } from './services/Students.service';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { SharedModuleModule } from './shared-module/shared-module.module';
import { IonicStorageModule } from '@ionic/storage';
import { InterceptorProvider } from './InterceptorProvider';
import { RouterHistoryService } from './services/routerHistory.service';
import { CommunicationService } from './services/communication.service';
import { UserService } from './services/users.service';
import { Storage } from '@ionic/storage';
import { SearchService } from './services/search.service';

@NgModule({
  declarations: [AppComponent],
  entryComponents: [],
  imports: [
    SharedModuleModule,
    BrowserModule,
    HttpClientModule,
    IonicStorageModule.forRoot(),
    IonicModule.forRoot(),
    AppRoutingModule,
  ],
  providers: [
    StatusBar,
    SplashScreen,
    InstitutService,
    HalaqaService,
    ClassesService,
    AchivementService,
    StudentsService,
    CommunicationService,
    RouterHistoryService,
    SearchService,
    UserService,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: InterceptorProvider,
      multi: true,
    },
    // { provide: RouteReuseStrategy, useClass: IonicRouteStrategy }
  ],
  bootstrap: [AppComponent],
})
export class AppModule {}
