import { Component, OnInit } from '@angular/core';
import { UserService } from 'src/app/services/users.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { UserModel } from 'src/app/auth/models/user.model';
import { ToastController } from '@ionic/angular';
import { Router } from '@angular/router';

@Component({
  selector: 'app-edit-profile',
  templateUrl: './edit-profile.page.html',
  styleUrls: ['./edit-profile.page.scss'],
})
export class EditProfilePage implements OnInit {
  UsersForm: FormGroup;
  userInfo: any;
  userId: any;
  user = new UserModel();

  // Change the new Value
  error_messages = {
    firstname: [{ type: 'required', message: 'First name is required.' }],
    lastname: [{ type: 'required', message: 'last name is required.' }],
    email: [
      { type: 'required', message: 'Email is required.' },
      { type: 'minlength', message: 'Email length.' },
      { type: 'maxlength', message: 'Email length.' },
      { type: 'required', message: 'please enter a valid email address.' },
    ],
  };

  constructor(
    public router: Router,
    public formBuilder: FormBuilder,
    public toastController: ToastController,
    private userservice: UserService
  ) {
    this.setUserInfo();
  }

  ngOnInit() {}

  async setUserInfo() {
    this.reloadUserInfo();
  }

  initUserForm(userInfo: any) {
    return this.formBuilder.group({
      firstname: [userInfo.firstname, Validators.required],
      lastname: [userInfo.lastname, Validators.required],
      email: [
        userInfo.email,
        Validators.compose([
          Validators.required,
          Validators.minLength(6),
          Validators.maxLength(30),
          Validators.pattern('^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+.[a-zA-Z0-9-.]+$'),
        ]),
      ],
      phone: [userInfo.phone],
    });
  }

  async reloadUserInfo() {
    this.userId = await this.userservice.getConnectedUserId();
    this.userservice.getUserById(this.userId).subscribe((res) => {
      this.userInfo = res.content[0];
      this.UsersForm = this.initUserForm(this.userInfo);
    });
  }

  UpdateUser(UsersForm) {
    this.userservice.updateUser(this.userId, UsersForm.value).subscribe(
      (data) => {
        if (data.code === '0000') {
          this.presentToast(data.message, 'success');
          this.GoBackToList();
        } else {
          this.presentToast(data.message, 'error');
        }
      },
      (err) => {
        this.presentToast(err, 'error');
        console.log('error', err);
      }
    );
  }

  async presentToast(msg: any, messagetype) {
    const toast = await this.toastController.create({
      message: msg,
      duration: 3000,
      position: 'bottom',
      color: messagetype === 'success' ? 'success' : 'danger',
    });
    toast.present();
  }

  GoBackToList() {
    this.router.navigate(['/profile']);
  }
}
