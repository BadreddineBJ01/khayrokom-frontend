import { Component, OnInit } from '@angular/core';
import { UserService } from '../services/users.service';
import { Router, NavigationEnd } from '@angular/router';
import { Storage } from '@ionic/storage';
import { InstitutService } from '../services/institut.service';



@Component({
  selector: 'app-profile',
  templateUrl: './profile.page.html',
  styleUrls: ['./profile.page.scss'],
})
export class ProfilePage implements OnInit {
  userInfo: any;
  userId: any;
  userInstitutes: any[] = [];
  connectdUserRole: any;
  logout: boolean;
  constructor(
    private userservice: UserService,
    private institutService: InstitutService, 
    private router: Router,
    private  storage: Storage) { 

    }
   ngOnInit() {   
      this.router.events.subscribe(res => {
        if (res instanceof NavigationEnd) {
          this.reloadUserInfo();
          this.getInstitutes();
        }
      });
  }

  async getInstitutes() {
    this.userInstitutes.length = 0;
    this.userId = await this.userservice.getConnectedUserId();
    this.connectdUserRole = await this.userservice.getUserRole(); 
 console.log('***************', this.userId);
      
    if(this.userId != null)
    {
      this.userservice.getUserById(this.userId)
      .subscribe(res => {
       res.content[0].institutesids.forEach(element => {
         this.institutService.getInstitutById(element).subscribe((res: any) => {
           if(res.content[0])
           {
            this.userInstitutes.push(res.content[0].InstituteName);
           };
         });
       });    
      });
    }else{
      console.log('user id undefined');
    }

  }


  async reloadUserInfo() {
    this.userId = await this.userservice.getConnectedUserId();
    console.log('user ids', this.userId);
    if(this.userId != null)
    {
      this.userservice
      .getUserById(this.userId)
      .subscribe(res => {
       this.userInfo = res.content[0];    
      });
    }

  }

  goToEditProfile() {
   this.router.navigateByUrl("/profile/edit-profile");
  }

}
