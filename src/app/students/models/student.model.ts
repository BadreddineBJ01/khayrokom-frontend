import {sharedModel} from '../../sharedModels/sharedModel';

export interface Student {
  firstname: string;
  lastname: string;
  gender: string;
  status: string;
  roomNumber: number;
  level: string;
  entryDate: Date;
  exitDate: Date;
  created_at: Date;
  classId: string;
  halaqaId: string;
}
export class StudentModel extends sharedModel {
  firstname: string;
  lastname: string;
  gender: string;
  status: string;
  roomNumber: number;
  level: string;
  entryDate: Date;
  exitDate:  Date;
  created_at: Date;
  classId: string;
  halaqaId: string;

  constructor(StudentEntity?: any) {
    StudentEntity = StudentEntity || {};
    super(StudentEntity);
    this.firstname = StudentEntity.firstname || '';
    this.lastname = StudentEntity.lastname || '';
    this.gender = StudentEntity.gender || ''; 
    this.status = StudentEntity.status || ''; 
    this.roomNumber = StudentEntity.roomNumber || ''; 
    this.level = StudentEntity.level || ''; 
    this.entryDate = StudentEntity.entryDate || ''; 
    this.exitDate = StudentEntity.exitDate || ''; 
    this.created_at = StudentEntity.created_at || ''; 
    this.classId = StudentEntity.classId || '';
    this.halaqaId = StudentEntity.halaqaId || '';
  }

}
