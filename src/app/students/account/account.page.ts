import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators, NgForm } from '@angular/forms';



@Component({
  selector: 'app-account',
  templateUrl: './account.page.html',
  styleUrls: ['./account.page.scss'],
})
export class AccountPage implements OnInit {
  StudentAccountForm: FormGroup;

  error_messages = {
    email: [
      { type: 'required', message: 'Email is required.' },
      { type: 'minlength', message: 'Email length.' },
      { type: 'maxlength', message: 'Email length.' },
      { type: 'required', message: 'please enter a valid email address.' }
    ],

    password: [
      { type: 'required', message: 'password is required.' },
      { type: 'minlength', message: 'password length.' },
      { type: 'maxlength', message: 'password length.' }
    ],
    confirmpassword: [
      { type: 'required', message: 'password is required.' },
      { type: 'minlength', message: 'password length.' },
      { type: 'maxlength', message: 'password length.' },
    ],
  };



  constructor(private formBuilder: FormBuilder) { }

  ngOnInit() {
  
    // get the user Id from thr route.
   // get the user object.

    this.StudentAccountForm = this.InitRegisterForm();
  }

  InitRegisterForm() {
    return this.formBuilder.group({
      email: ['', Validators.compose([
        Validators.required,
        Validators.minLength(6),
        Validators.maxLength(30),
        Validators.pattern('^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+.[a-zA-Z0-9-.]+$')
      ])],
      password: ['', Validators.compose([
        Validators.required,
        Validators.minLength(6),
        Validators.maxLength(30)
      ])],
      confirmpassword: ['', Validators.compose([
        Validators.required,
        Validators.minLength(6),
        Validators.maxLength(30)
      ])]
    }, { 
      validators: this.password.bind(this)
    });
  }

  password(formGroup: FormGroup) {
    const { value: password } = formGroup.get('password');
    const { value: confirmPassword } = formGroup.get('confirmpassword');
    return password === confirmPassword ? null : { passwordNotMatch: true };
  }

  registerUser(UserToAdd: NgForm) {
    // console.log(UserToAdd);

    // this.userService.registerUser(UserToAdd).subscribe(
    //   res => {
    //     if (res.code === '0000') {
    //       this.presentToast(res.message, 'success');
    //       this.resetForm();
    //       this.router.navigate(['/auth']);
    //       console.log('res 1', res);
    //     } else {
    //       this.presentToast(res.message, 'error');
    //       console.log('res 2', res);
    //     }
    //   },
    //   err => {
    //     console.log('error ---> ', err);
    //     this.presentToast(err, 'error');
    //   }
    // );
  }

}
