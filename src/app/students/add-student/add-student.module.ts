import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { AddStudentPageRoutingModule } from './add-student-routing.module';

import { SharedModuleModule } from 'src/app/shared-module/shared-module.module';
import { AddStudentPage } from './add-student.page';

@NgModule({
  imports: [
    SharedModuleModule,
    CommonModule,
    FormsModule,
    IonicModule,
    AddStudentPageRoutingModule
  ],
  declarations: [AddStudentPage]
})
export class AddStudentPageModule {}
