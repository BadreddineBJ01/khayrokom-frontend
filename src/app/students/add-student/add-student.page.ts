import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators, NgForm } from '@angular/forms';
import { StudentsService } from 'src/app/services/Students.service';
import { Router, ActivatedRoute, NavigationEnd } from '@angular/router';
import { StudentModel } from '../models/student.model';
import { ClassesService } from 'src/app/services/classes.service';
import { switchMap, map } from 'rxjs/operators';
import { CommunicationService } from 'src/app/services/communication.service';

@Component({
  selector: 'app-add-student',
  templateUrl: './add-student.page.html',
  styleUrls: ['./add-student.page.scss'],
})
export class AddStudentPage implements OnInit {
  StudentForm: FormGroup;
  StudentToEdit = new StudentModel();
  EditStudentInfo = false;
  studentId: any;
  classList: any;
  HalaqaId: any;
  InstitutId: any;
  constructor(
    public formBuilder: FormBuilder,
    public router: Router,
    public studentsService: StudentsService,
    private activatedRoute: ActivatedRoute,
    private classService: ClassesService,
    private communicationService: CommunicationService
  ) {}

  ngOnInit() {
    this.HalaqaId = this.communicationService.HalaqaId;
    this.InstitutId = this.communicationService.InstitutId;
    this.initForm();
    this.router.events
      .pipe(
        switchMap((navigation) =>
          this.activatedRoute.params.pipe(
            map((params) => ({ navigation, params }))
          )
        )
      )
      .subscribe((result) => {
        if (result.navigation instanceof NavigationEnd) {
          const urlfragment = result.navigation.url.split('/')[2];
          if (
            urlfragment.toLowerCase().includes('add') &&
            result.params.id !== undefined &&
            result.params !== {}
          ) {
            this.HalaqaId = this.communicationService.HalaqaId =
              result.params.id;
            this.EditStudentInfo = false;
            this.initForm();
          } else if (
            urlfragment.toLowerCase().includes('edit') &&
            result.params.id !== undefined &&
            result.params !== {}
          ) {
            this.GetStudentById(result.params.id);
            this.studentId = result.params.id;
            this.EditStudentInfo = true;
          } else {
            console.log('error id undefined');
          }
        }
      });
    this.getAllInstitutClassesById();
  }

  getAllInstitutClassesById() {
    this.classService.getAllInstitutClassesById(this.InstitutId).subscribe(
      (data) => {
        if (data.code === '0000') {
          this.classList = data.content;
        } else {
          console.log('error');
        }
      },
      (err) => {
        console.log('subscription error', err);
      }
    );
  }

  GetStudentById(StudentId: any) {
    this.studentsService.getStudentById(StudentId).subscribe(
      (data) => {
        if (data.code === '0000') {
          console.log('data content', data.content[0]);
          this.StudentToEdit = new StudentModel(data.content[0]);
          console.log('this.StudentToEdit', this.StudentToEdit);
          this.initForm();
        } else {
          console.log('error', data);
        }
      },
      (err) => {
        console.log('error', err);
      }
    );
  }

  initForm() {
    this.StudentForm = this.formBuilder.group({
      firstname: [this.StudentToEdit.firstname, Validators.required],
      lastname: [this.StudentToEdit.lastname, Validators.required],
      gender: [this.StudentToEdit.gender],
      status: [this.StudentToEdit.status],
      roomNumber: [this.StudentToEdit.roomNumber],
      level: [this.StudentToEdit.level],
      entryDate: [this.StudentToEdit.entryDate],
      exitDate: [this.StudentToEdit.exitDate],
      classId: [this.StudentToEdit.classId],
    });
  }

  addStudent(StudentForm: FormGroup) {
    const StudentToAdd = {
      firstname: '',
      lastname: '',
      gender: '',
      status: '',
      roomNumber: '',
      level: '',
      entryDate: '',
      exitDate: '',
      created_at: '',
      classId: '',
      halaqaId: '',
    };
    Object.assign(StudentToAdd, StudentForm.value);
    StudentToAdd.halaqaId = this.HalaqaId;

    console.log('StudentToAdd', StudentToAdd);

    this.studentsService.addStudent(StudentToAdd).subscribe(
      (data) => {
        if (data.code === '0000') {
          console.log('student added with success');
          this.GoBackToList();
        } else {
          console.log('error');
        }
      },
      (err) => {
        console.log('error', err);
      }
    );
  }

  UpdateStudent(StudentForm: FormGroup) {
    const StudentToAdd = {
      firstname: '',
      lastname: '',
      gender: '',
      status: '',
      roomNumber: '',
      level: '',
      entryDate: '',
      exitDate: '',
      created_at: '',
      classId: '',
      halaqaId: '',
    };
    Object.assign(StudentToAdd, StudentForm.value);
    StudentToAdd.halaqaId = this.HalaqaId;
    this.studentsService.updateStudent(this.studentId, StudentToAdd).subscribe(
      (data) => {
        if (data.code === '0000') {
          console.log('student updated with success');
          this.GoBackToList();
        } else {
          console.log('error update');
        }
      },
      (err) => {
        console.log('error', err);
      }
    );
  }

  GoBackToList() {
    this.router.navigate(['/students/students-list/' + this.HalaqaId]);
  }
}
