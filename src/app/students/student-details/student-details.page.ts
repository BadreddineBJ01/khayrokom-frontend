import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Student, StudentModel } from '../models/student.model';
import { StudentsService } from 'src/app/services/Students.service';
import { ClassesService } from 'src/app/services/classes.service';

@Component({
  selector: 'app-student-details',
  templateUrl: './student-details.page.html',
  styleUrls: ['./student-details.page.scss'],
})
export class StudentDetailsPage implements OnInit {
  StudentId: any;
  studentDetails: any;
  studentClass: any;
  constructor(private activatedRoute: ActivatedRoute,
              private studentsService: StudentsService,
              private classesService: ClassesService) { }

  ngOnInit() {
    this.StudentId = this.activatedRoute.snapshot.paramMap.get('id');
    this.getStudentById(this.StudentId);
	//this.studentClass = 
  }

  getStudentById(StudentId: any) {
    this.studentsService.getStudentById(StudentId).subscribe(data => {
     if (data.code === '0000') {
       console.log(data.content);
       this.studentDetails = data.content[0];
	   this.getClassById(this.studentDetails.classId);
     } else {
      console.log('error', data);
     }
    }, (err) => {
      console.log('error', err);
    });
   }
   
   
   getClassById(ClassId: any) {
    this.classesService.getClassById(ClassId).subscribe(data => {
		console.log("Msg" ,data);
     if (data.code === '0000') {
       console.log(data.content);
       this.studentClass = data.content[0];
     } else {
      console.log('error', data);
     }
    }, (err) => {
      console.log('error', err);
    });
   }
}
