import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { StudentsListPageRoutingModule } from './students-list-routing.module';

import { StudentsListPage } from './students-list.page';
import { SharedModuleModule } from 'src/app/shared-module/shared-module.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    SharedModuleModule,
    StudentsListPageRoutingModule
  ],
  declarations: [StudentsListPage]
})
export class StudentsListPageModule {}
