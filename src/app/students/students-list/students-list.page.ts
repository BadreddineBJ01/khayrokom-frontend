import { Component, OnInit } from '@angular/core';
import { StudentsService } from 'src/app/services/Students.service';
import { Router, ActivatedRoute, NavigationEnd } from '@angular/router';
import { AlertController, ModalController, LoadingController } from '@ionic/angular';
import { StudentDetailsPage } from '../student-details/student-details.page';
import { CommunicationService } from 'src/app/services/communication.service';
import { UserService } from 'src/app/services/users.service';
import { FormGroup, FormBuilder } from '@angular/forms';
import { ClassesService } from 'src/app/services/classes.service';
import { SearchService } from 'src/app/services/search.service';

@Component({
  selector: 'app-students-list',
  templateUrl: './students-list.page.html',
  styleUrls: ['./students-list.page.scss'],
})
export class StudentsListPage implements OnInit {
  studentsList: any;
  HalaqaId: any;
  connectedUserRole: any;
  StudentFilterForm: FormGroup;
  classList: any[] = [];
  InstitutId: any;
  nodataFound = false;
  constructor(
    private studentsService: StudentsService,
    private router: Router,
    private userService: UserService,
    private activatedRoute: ActivatedRoute,
    private alertCtrl: AlertController,
    private fb: FormBuilder,
    private searchService: SearchService,
    private classService: ClassesService,
    public modalController: ModalController,
    public communicationService: CommunicationService,
    private loadingController: LoadingController) { }

  ngOnInit() {
    this.InstitutId = this.communicationService.InstitutId;
    console.log('institutId', this.InstitutId);
    this.activatedRoute.params.subscribe((params: any) => {
      if (params !== {} && params.id !== undefined) {
        this.HalaqaId = this.communicationService.HalaqaId = params.id;
      }
    });

    this.router.events.subscribe(res => {
      if (res instanceof NavigationEnd) {
        this.getAllHalaqaStudents();
        this.setUserRole();
      }
    });

    this.initForm();
    this.getAllInstitutClassesById();
  }

  initForm(){
    this.StudentFilterForm =  this.fb.group({
      Relatedclass: ['']
    });
  }

  async SelectClass(event: any) {
    const loading = await this.loadingController.create({
      message: 'Loading...'
    });
    await loading.present();

   console.log('event ---->', event);
   let classId = event.detail.value;
   
    if (classId != 'All')
   { 
      this.searchService.StudentByClass(this.HalaqaId ,classId).subscribe((res: any) => {
      if (res.code == "0000")
      {
       this.studentsList = res.content;
       if(this.studentsList.length == 0)
       {
         this.nodataFound = true;
       }else{
        this.nodataFound = false;         
       }
       loading.dismiss();
      }else {
        console.log('error');
        loading.dismiss();
      }
      },(err) => {
        console.log('error');
        loading.dismiss();
      });
   } else if (classId === 'All') {
     this.getAllHalaqaStudents();
     loading.dismiss();
   } else{
     console.log('error');
     loading.dismiss();
   }
  }

  getAllInstitutClassesById() {
    this.classService.getAllInstitutClassesById(this.InstitutId).subscribe(
      data => {
        if (data.code === '0000') {
          this.classList = data.content;
          console.log("class list --> ", this.classList);
        } else {
          console.log('error');
        }
      },
      err => {
        console.log('subscription error', err);
      }
    );
  }



  createStudentAccount(studentId: any)
  {
  this.router.navigate(['/students/account/'+studentId]);
  }
  
  async setUserRole() {
    this.connectedUserRole = await this.userService.getUserRole();
  }

  goToAddStudent() {
  this.router.navigate(['/students/add-student/'+ this.HalaqaId]);    
  }

  AddStudentAchievement(studentId: any)
  {
    this.router.navigate(['/achievements/achivement-list/' + studentId]);
  }
  
  async getAllHalaqaStudents() {
    this.studentsService.getAllHalqaStudents(this.HalaqaId).subscribe(
      data => {
        if (data.code === '0000') {
          this.studentsList = data.content;
          if(this.studentsList.length == 0){
            this.nodataFound = true;
          }else{
            this.nodataFound = false;         
           }   
        } else {
        }
      },
      err => {
        console.log('subscription error', err);
      }
    );
  }

  editStudent(studentId: any) {
    this.router.navigate(['students/edit-student/' + studentId]);
  }

  delete(studentId: any) {
    this.studentsService.deleteStudent(studentId).subscribe(
      data => {
        if (data.code === '0000') {
          console.log('delete data', data);
        } else {
          console.log('error');
        }
      },
      err => {
        console.log('error');
      }
    );
  }

  Studentdetails(studentId: any) {
   this.router.navigate(['students/student-details/' + studentId]);
  }

  async presentConfirmDelete(studentId: any) {
    const alert = this.alertCtrl.create({
      message: 'Do you want to delet that student',
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          handler: () => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'Confirm',
          handler: () => {
            this.delete(studentId);
            this.ReloadList();
          }
        }
      ]
    });
    (await alert).present();
  }

  ReloadList() {
    this.getAllHalaqaStudents();
  }
}
