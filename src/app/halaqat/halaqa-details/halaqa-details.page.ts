import { Component, OnInit } from '@angular/core';
import { HalaqaService } from 'src/app/services/halaqa.service';
import { ActivatedRoute } from '@angular/router';
import { Halaqa, HalaqaModel } from '../models/Halaqa.model';


@Component({
  selector: 'app-halaqa-details',
  templateUrl: './halaqa-details.page.html',
  styleUrls: ['./halaqa-details.page.scss'],
})
export class HalaqaDetailsPage implements OnInit {
  HalaqaId: any;
  halaqaDetails: any;
  constructor(private activatedRoute: ActivatedRoute,
              private halaqaService: HalaqaService) { }

  ngOnInit() {
    this.HalaqaId = this.activatedRoute.snapshot.paramMap.get('id');
    this.getHalaqaById(this.HalaqaId);
  }

  getHalaqaById(HalaqaId: any) {
    this.halaqaService.getHalaqaById(HalaqaId).subscribe(data => {
     if (data.code === '0000') {
       console.log(data.content);
       this.halaqaDetails = data.content[0];
     } else {
      console.log('error', data);
     }
    }, (err) => {
      console.log('error', err);
    });
   }

   // format details.

}