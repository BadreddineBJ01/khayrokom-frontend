import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { HalaqaDetailsPage } from './halaqa-details.page';

const routes: Routes = [
  {
    path: '',
    component: HalaqaDetailsPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class HalaqaDetailsPageRoutingModule {}
