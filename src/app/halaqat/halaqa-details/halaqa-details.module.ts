import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { HalaqaDetailsPageRoutingModule } from './halaqa-details-routing.module';

import { HalaqaDetailsPage } from './halaqa-details.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    HalaqaDetailsPageRoutingModule
  ],
  declarations: [HalaqaDetailsPage]
})
export class HalaqaDetailsPageModule {}
