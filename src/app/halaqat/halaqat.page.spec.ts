import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { HalaqatPage } from './halaqat.page';

describe('HalaqatPage', () => {
  let component: HalaqatPage;
  let fixture: ComponentFixture<HalaqatPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HalaqatPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(HalaqatPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
