import {sharedModel} from '../../sharedModels/sharedModel';

export interface Halaqa {
  Halaqaname: string;
}

export class HalaqaModel extends sharedModel {
  Halaqaname: string;

  constructor(HalaqaEntity?: any) {
    HalaqaEntity = HalaqaEntity || {};
    super(HalaqaEntity);
    this.Halaqaname = HalaqaEntity.Halaqaname || '';

  }

}