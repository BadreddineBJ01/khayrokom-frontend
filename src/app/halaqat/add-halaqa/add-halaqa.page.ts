import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators, NgForm } from '@angular/forms';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HalaqaService } from 'src/app/services/halaqa.service';
import { Router, ActivatedRoute, NavigationEnd } from '@angular/router';
import { HalaqaModel } from '../models/Halaqa.model';
import { switchMap, map } from 'rxjs/operators';
import { UserService } from 'src/app/services/users.service';
import { CommunicationService } from 'src/app/services/communication.service';

@Component({
  selector: 'app-add-halaqa',
  templateUrl: './add-halaqa.page.html',
  styleUrls: ['./add-halaqa.page.scss'],
})
export class AddHalaqaPage implements OnInit {
  HalaqaForm: FormGroup;
  HalaqaToEdit = new HalaqaModel();
  EditHalaqaInfo = false;
  halaqaId: any;
  InstitutId: any;
  connectedUserId: any;

  // Form error types

  error_messages = {
    Halaqaname: [{ type: 'required', message: 'Halaqa name is required.' }],
  };

  constructor(
    public userService: UserService,
    public formBuilder: FormBuilder,
    public router: Router,
    public halaqaService: HalaqaService,
    private activatedRoute: ActivatedRoute,
    private communicationService: CommunicationService
  ) {}

  ngOnInit() {
    this.InstitutId = this.communicationService.InstitutId;
    this.getConnectdUserId();
    this.initForm();
    this.router.events
      .pipe(
        switchMap((navigation) =>
          this.activatedRoute.params.pipe(
            map((params) => ({ navigation, params }))
          )
        )
      )
      .subscribe((result) => {
        if (result.navigation instanceof NavigationEnd) {
          const urlfragment = result.navigation.url.split('/')[2];
          if (
            urlfragment.toLowerCase().includes('add') &&
            result.params.id !== undefined &&
            result.params !== {}
          ) {
            //   this.InstitutId = result.params.id;
            this.EditHalaqaInfo = false;
            this.initForm();
          } else if (
            urlfragment.toLowerCase().includes('edit') &&
            result.params.id !== undefined &&
            result.params !== {}
          ) {
            this.GetHalaqaById(result.params.id);
            this.halaqaId = result.params.id;
            this.EditHalaqaInfo = true;
          } else {
            console.log('error id undefined');
          }
        }
      });
  }

  async getConnectdUserId() {
    this.connectedUserId = await this.userService.getConnectedUserId();
    console.log('user id ', this.connectedUserId);
  }

  GetHalaqaById(HalaqaId: any) {
    console.log('params --->', HalaqaId);
    this.halaqaService.getHalaqaById(HalaqaId).subscribe(
      (data) => {
        if (data.code === '0000') {
          console.log('data content', data.content[0]);
          this.HalaqaToEdit = new HalaqaModel(data.content[0]);
          console.log('this.HalaqaToEdit', this.HalaqaToEdit);
          this.initForm();
        } else {
          console.log('error', data);
        }
      },
      (err) => {
        console.log('error', err);
      }
    );
  }

  initForm() {
    this.HalaqaForm = this.formBuilder.group({
      Halaqaname: [this.HalaqaToEdit.Halaqaname, Validators.required],
    });
  }

  AddHalaqa(HalaqaForm: FormGroup) {
    const HalaqaToAdd = { Halaqaname: '', InstitutId: '', CheikhId: '' };
    HalaqaToAdd.Halaqaname = HalaqaForm.value.Halaqaname;
    HalaqaToAdd.CheikhId = this.connectedUserId;
    HalaqaToAdd.InstitutId = this.InstitutId;

    console.log('ngForm', HalaqaToAdd);
    this.halaqaService.addHalaqa(HalaqaToAdd).subscribe(
      (data) => {
        if (data.code === '0000') {
          console.log('halaqa added with success');
          this.GoBackToList();
        } else {
          console.log('error');
        }
      },
      (err) => {
        console.log('error', err);
      }
    );
  }

  UpdateHalaqa(HalaqaForm: FormGroup) {
    const HalaqaToAdd = { Halaqaname: '', InstitutId: '', CheikhId: '' };
    HalaqaToAdd.Halaqaname = HalaqaForm.value.Halaqaname;
    HalaqaToAdd.CheikhId = this.connectedUserId;
    HalaqaToAdd.InstitutId = this.InstitutId;

    console.log('HalaqaForm.value', HalaqaToAdd, this.halaqaId);
    this.halaqaService.updateHalaqa(this.halaqaId, HalaqaToAdd).subscribe(
      (data) => {
        if (data.code === '0000') {
          console.log('halaqa updated with success');
          this.GoBackToList();
        } else {
          console.log('error update');
        }
      },
      (err) => {
        console.log('error', err);
      }
    );
  }

  GoBackToList() {
    this.router.navigate(['/halaqat/halaqat-list/' + this.InstitutId]);
  }
}
