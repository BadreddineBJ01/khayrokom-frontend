import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';

import { AddHalaqaPageRoutingModule } from './add-halaqa-routing.module';

import { AddHalaqaPage } from './add-halaqa.page';
import { SharedModuleModule } from 'src/app/shared-module/shared-module.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    AddHalaqaPageRoutingModule,
    SharedModuleModule
  ],
  declarations: [AddHalaqaPage]
})
export class AddHalaqaPageModule {}
