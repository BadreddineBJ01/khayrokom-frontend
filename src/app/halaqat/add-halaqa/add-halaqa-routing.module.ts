import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AddHalaqaPage } from './add-halaqa.page';

const routes: Routes = [
  {
    path: '',
    component: AddHalaqaPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AddHalaqaPageRoutingModule {}
