import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { AddHalaqaPage } from './add-halaqa.page';

describe('AddHalaqaPage', () => {
  let component: AddHalaqaPage;
  let fixture: ComponentFixture<AddHalaqaPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddHalaqaPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(AddHalaqaPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
