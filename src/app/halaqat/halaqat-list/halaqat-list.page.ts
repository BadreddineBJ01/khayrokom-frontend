import { Component, OnInit } from '@angular/core';
import { HalaqaService } from 'src/app/services/halaqa.service';
import { Router, ActivatedRoute, NavigationEnd } from '@angular/router';
import { AlertController, ModalController, LoadingController } from '@ionic/angular';
import { HalaqaDetailsPage } from '../halaqa-details/halaqa-details.page';
import { UserService } from 'src/app/services/users.service';
import { CommunicationService } from 'src/app/services/communication.service';

@Component({
  selector: 'app-halaqat-list',
  templateUrl: './halaqat-list.page.html',
  styleUrls: ['./halaqat-list.page.scss'],
})


export class HalaqatListPage implements OnInit {
  InstitutId: any;
  halaqatList: any;
  connectedUserId: any;
  connectedUserRole: any;
  constructor(
    private halaqaService: HalaqaService,
    private router: Router,
    private userService: UserService,
    private activatedRoute: ActivatedRoute,
    private alertCtrl: AlertController,
    public modalController: ModalController,
    private loadingController: LoadingController, 
    private communicationService: CommunicationService
  ) {}

  ngOnInit() {  
    this.activatedRoute.params.subscribe((params: any) => {
      if (params !== {} && params.id !== undefined) {
        this.InstitutId = this.communicationService.InstitutId = params.id;
      } else {
        console.log('error id not found');
      }
    });

    this.router.events.subscribe(res => {
      if (res instanceof NavigationEnd) {
        this.getAllCheickHalaqatById();
      }
    });
    this.getConnectdUserId();
    this.setUserRole();
  }

  async setUserRole() {
    this.connectedUserRole = await this.userService.getUserRole();
  }

  async getConnectdUserId() {
    this.connectedUserId = await this.userService.getConnectedUserId();
    console.log('user id ', this.connectedUserId);
  }

  goToAddStudents(halaqaId: any){
   this.router.navigate(['students/students-list/' + halaqaId]);
  }

  goToAddHalaqa(){
   this.router.navigate(['halaqat/add-halaqa/' + this.InstitutId]); 
  }

  async getAllCheickHalaqatById() {
    const loading = await this.loadingController.create({
      message: 'Loading...'
    });
    await loading.present();

    this.halaqaService.getAllCheickHalaqats(this.InstitutId, this.connectedUserId).subscribe(
      data => {
        if (data.code === '0000') {
          console.log('data', data);
          this.halaqatList = data.content;
          loading.dismiss();
        } else {
          console.log('error');
          loading.dismiss();
        }
      },
      err => {
        console.log('subscription error', err);
        loading.dismiss();
      }
    );
  }

  edithalaqa(halaqaId: any) {
    this.router.navigate(['halaqat/edit-halaqa/' + halaqaId]);
  }

  delete(halaqaId: any) {
    this.halaqaService.deleteHalaqa(halaqaId).subscribe(
      data => {
        if (data.code === '0000') {
          console.log('delete data', data);
        } else {
          console.log('error');
        }
      },
      err => {
        console.log('error');
      }
    );
  }

  Halaqadetails(halaqaId: any) {
   this.router.navigate(['halaqat/halaqa-details/' + halaqaId]);
  }

  async presentConfirmDelete(halaqaId: any) {
    const alert = this.alertCtrl.create({
      message: 'Do you want to delet this halaqa',
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          handler: () => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'Confirm',
          handler: () => {
            this.delete(halaqaId);
            this.ReloadList();
          }
        }
      ]
    });
    (await alert).present();
  }

  ReloadList() {
    this.getAllCheickHalaqatById();
  }
}