import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { HalaqatListPage } from './halaqat-list.page';

const routes: Routes = [
  {
    path: '',
    component: HalaqatListPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class HalaqatListPageRoutingModule {}
