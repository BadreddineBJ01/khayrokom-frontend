import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { HalaqatListPage } from './halaqat-list.page';

describe('HalaqatListPage', () => {
  let component: HalaqatListPage;
  let fixture: ComponentFixture<HalaqatListPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HalaqatListPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(HalaqatListPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
