import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { HalaqatListPageRoutingModule } from './halaqat-list-routing.module';

import { HalaqatListPage } from './halaqat-list.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    HalaqatListPageRoutingModule
  ],
  declarations: [HalaqatListPage]
})
export class HalaqatListPageModule {}
