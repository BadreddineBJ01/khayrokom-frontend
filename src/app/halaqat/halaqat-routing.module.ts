import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { HalaqatPage } from './halaqat.page';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'halaqat-list',
    pathMatch: 'full'
  },
  {
    path: 'halaqat-list/:id',
    loadChildren: () => import('./halaqat-list/halaqat-list.module').then( m => m.HalaqatListPageModule)
  },
  {
    path: 'add-halaqa/:id',
    loadChildren: () => import('./add-halaqa/add-halaqa.module').then( m => m.AddHalaqaPageModule)
  },
  {
    path: 'halaqa-details/:id',
    loadChildren: () => import('./halaqa-details/halaqa-details.module').then( m => m.HalaqaDetailsPageModule)
  },
  {
    path: 'edit-halaqa/:id',
    loadChildren: () => import('./add-halaqa/add-halaqa.module').then( m => m.AddHalaqaPageModule)
  },
  {
    path: '**',
    redirectTo: 'halaqat-list'
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class HalaqatPageRoutingModule {}
