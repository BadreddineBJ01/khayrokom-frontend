import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { HalaqatPageRoutingModule } from './halaqat-routing.module';

import { HalaqatPage } from './halaqat.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    HalaqatPageRoutingModule
  ],
  declarations: [HalaqatPage]
})
export class HalaqatPageModule {}
