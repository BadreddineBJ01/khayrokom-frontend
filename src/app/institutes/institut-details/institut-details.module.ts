import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { InstitutDetailsPageRoutingModule } from './institut-details-routing.module';

import { InstitutDetailsPage } from './institut-details.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    InstitutDetailsPageRoutingModule
  ],
  declarations: [InstitutDetailsPage]
})
export class InstitutDetailsPageModule {}
