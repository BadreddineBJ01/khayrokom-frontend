import { Component, OnInit } from '@angular/core';
import { InstitutService } from 'src/app/services/institut.service';
import { ActivatedRoute } from '@angular/router';
import { Institut, InstitutModel } from '../models/Institut.model';

@Component({
  selector: 'app-institut-details',
  templateUrl: './institut-details.page.html',
  styleUrls: ['./institut-details.page.scss'],
})
export class InstitutDetailsPage implements OnInit {
  InstitutId: any;
  institutDetails: any;
  constructor(private activatedRoute: ActivatedRoute,
              private institutService: InstitutService) { }

  ngOnInit() {
    this.InstitutId = this.activatedRoute.snapshot.paramMap.get('id');
    this.getInstitutById(this.InstitutId);
  }

  getInstitutById(InstitutId: any) {
    this.institutService.getInstitutById(InstitutId).subscribe(data => {
     if (data.code === '0000') {
       console.log(data.content);
       this.institutDetails = data.content[0];
     } else {
      console.log('error', data);
     }
    }, (err) => {
      console.log('error', err);
    });
   }

   // format details.

}
