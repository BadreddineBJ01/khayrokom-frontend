import {sharedModel} from '../../sharedModels/sharedModel';

export interface Institut {
  InstituteName: string;
  country: string;
  city: string;
}

export class InstitutModel extends sharedModel {
  InstituteName: string;
  country: string;
  city: string;

  constructor(InstitutEntity?: any) {
    InstitutEntity = InstitutEntity || {};
    super(InstitutEntity);
    this.InstituteName = InstitutEntity.InstituteName || '';
    this.country = InstitutEntity.country || '';
    this.city = InstitutEntity.city || ''; 
  }

}
