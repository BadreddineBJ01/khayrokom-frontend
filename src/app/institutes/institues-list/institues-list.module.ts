import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { InstituesListPageRoutingModule } from './institues-list-routing.module';

import { InstituesListPage } from './institues-list.page';
import { InstitutDetailsPage } from '../institut-details/institut-details.page';
import { InstitutDetailsPageModule } from '../institut-details/institut-details.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    InstituesListPageRoutingModule
  ],
  declarations: [InstituesListPage]
})
export class InstituesListPageModule {}
