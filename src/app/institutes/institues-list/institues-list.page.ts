import { Component, OnInit } from "@angular/core";
import { InstitutService } from "src/app/services/institut.service";
import { Router, NavigationEnd } from "@angular/router";
import {
  AlertController,
  ModalController,
  LoadingController,
  ToastController
} from "@ionic/angular";
import { InstitutDetailsPage } from "../institut-details/institut-details.page";
import { UserService } from "src/app/services/users.service";
import { CommunicationService } from 'src/app/services/communication.service';



@Component({
  selector: "app-institues-list",
  templateUrl: "./institues-list.page.html",
  styleUrls: ["./institues-list.page.scss"]
})
export class InstituesListPage implements OnInit {
  institutesList: any;
  connectedUserRole: any;
  constructor(
    private communicationService: CommunicationService,
    private institutService: InstitutService,
    private router: Router,
    private userService: UserService,
    private alertCtrl: AlertController,
    public  modalController: ModalController,
    private loadingController: LoadingController,
    private toastController: ToastController
  ) {}

  ngOnInit() {
    this.router.events.subscribe(res => {
      if (res instanceof NavigationEnd) {
        this.getAllInstitutes();
        this.setUserRole();
      }
    });
  }

  async setUserRole() {
    this.connectedUserRole = await this.userService.getUserRole();
  }

  async goToClasses(institutId: any) {
    this.communicationService.InstitutId = institutId;
    let userRole = await this.userService.getUserRole();
    switch (userRole) {
      case "admin": {
        this.router.navigate(["classes/classe-list/" + institutId]);
        break;
      }
      case "cheick": {
        this.router.navigate(["/halaqat/halaqat-list/" + institutId]);
        break;
      }
      default: {
        console.log("user role is not setted");
      }
    }
  }

  async getAllInstitutes() {
    const loading = await this.loadingController.create({
      message: "Loading..."
    });
    await loading.present();

    this.institutService.getAllInstitutes().subscribe(
      data => {
        if (data.code === "0000") {
          console.log("data", data);
          this.institutesList = data.content;
          loading.dismiss();
        } else {
          console.log("error");
          loading.dismiss();
        }
      },
      err => {
        console.log("subscription error", err);
        loading.dismiss();
      }
    );
  }

  editInstitut(institutId: any) {
    this.router.navigate(["institutes/edit-institut/" + institutId]);
  }

  delete(institutId: any) {
    this.institutService.deleteInstitut(institutId).subscribe(
      data => {
        if (data.code === "0000") {
          this.presentToast(data.message, "success");
          console.log("delete data", data);
        } else {
          this.presentToast(data.message, "error");
          console.log("error");
        }
      },
      err => {
        this.presentToast(err, "error");
        console.log("error");
      }
    );
  }

  Institutdetails(institutId: any) {
    this.router.navigate(["institutes/institut-details/" + institutId]);
  }

  async presentConfirmDelete(institutId: any) {
    const alert = this.alertCtrl.create({
      message: "Do you want to delet this institut",
      buttons: [
        {
          text: "Cancel",
          role: "cancel",
          handler: () => {
            console.log("Cancel clicked");
          }
        },
        {
          text: "Confirm",
          handler: () => {
            this.delete(institutId);
            this.ReloadList();
          }
        }
      ]
    });
    (await alert).present();
  }

  ReloadList() {
    this.getAllInstitutes();
  }

  async presentToast(msg: any, messagetype) {
    const toast = await this.toastController.create({
      message: msg,
      duration: 3000,
      position: "bottom",
      color: messagetype === "success" ? "success" : "danger"
    });
    toast.present();
  }
}
