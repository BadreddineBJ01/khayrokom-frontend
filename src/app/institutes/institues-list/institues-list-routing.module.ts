import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { InstituesListPage } from './institues-list.page';
import { InstitutDetailsPageModule } from '../institut-details/institut-details.module';

const routes: Routes = [
  {
    path: '',
    component: InstituesListPage
  }
];

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [RouterModule],
})
export class InstituesListPageRoutingModule {}
