import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { InstituesListPage } from './institues-list.page';

describe('InstituesListPage', () => {
  let component: InstituesListPage;
  let fixture: ComponentFixture<InstituesListPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InstituesListPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(InstituesListPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
