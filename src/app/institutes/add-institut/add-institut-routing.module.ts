import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AddInstitutPage } from './add-institut.page';

const routes: Routes = [
  {
    path: '',
    component: AddInstitutPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AddInstitutPageRoutingModule {}
