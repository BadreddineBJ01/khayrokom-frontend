import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators, NgForm } from '@angular/forms';
import { InstitutService } from 'src/app/services/institut.service';
import { Router, ActivatedRoute } from '@angular/router';
import { InstitutModel } from '../models/Institut.model';
import { ToastController } from '@ionic/angular';
import { countries } from '../models/Countries';

@Component({
  selector: 'app-add-institut',
  templateUrl: './add-institut.page.html',
  styleUrls: ['./add-institut.page.scss'],
})
export class AddInstitutPage implements OnInit {
  InstitutForm: FormGroup;
  InstitutToEdit = new InstitutModel();
  EditInstitutInfo = false;
  insitutId: any;
  countriesArray: any[] = [];
  error_messages = {
    InstituteName: [
      { type: 'required', message: 'Institut name is required.' },
    ],
  };

  constructor(
    private toastController: ToastController,
    public formBuilder: FormBuilder,
    public router: Router,
    public institutService: InstitutService,
    private activatedRoute: ActivatedRoute
  ) {}

  ngOnInit() {
    this.loadAllCountries();
    this.initForm();
    this.activatedRoute.params.subscribe((params: any) => {
      console.log('params', params);
      if (params !== {} && params.id !== undefined) {
        this.GetInstitutById(params.id);
        this.insitutId = params.id;
        this.EditInstitutInfo = true;
      } else {
        this.EditInstitutInfo = false;
        this.initForm();
      }
    });
  }

  loadAllCountries() {
    this.countriesArray = countries;
  }

  GetInstitutById(InstitutId: any) {
    console.log('params --->', InstitutId);
    this.institutService.getInstitutById(InstitutId).subscribe(
      (data) => {
        if (data.code === '0000') {
          console.log('data content', data.content[0]);
          this.InstitutToEdit = new InstitutModel(data.content[0]);
          console.log('this.InstitutToEdit', this.InstitutToEdit);
          this.initForm();
        } else {
          console.log('error', data);
        }
      },
      (err) => {
        console.log('error', err);
      }
    );
  }

  initForm() {
    this.InstitutForm = this.formBuilder.group({
      InstituteName: [this.InstitutToEdit.InstituteName, Validators.required],
      country: [this.InstitutToEdit.country],
      city: [this.InstitutToEdit.city],
    });
  }

  addInstitut(InstitutForm: FormGroup) {
    console.log('ngForm', InstitutForm.value);
    this.institutService.addInstitut(InstitutForm.value).subscribe(
      (data) => {
        if (data.code === '0000') {
          this.presentToast(data.message, 'success');
          console.log('institut added with success');
          this.GoBackToList();
        } else {
          this.presentToast(data.message, 'error');
          console.log('error');
        }
      },
      (err) => {
        this.presentToast(err, 'error');
        console.log('error', err);
      }
    );
  }

  UpdateInstitut(InstitutForm: FormGroup) {
    console.log('InstittForm.value', InstitutForm.value, this.insitutId);
    this.institutService
      .UpdateInstitut(this.insitutId, InstitutForm.value)
      .subscribe(
        (data) => {
          if (data.code === '0000') {
            this.presentToast(data.message, 'success');
            console.log('institut updated with success');
            this.GoBackToList();
          } else {
            this.presentToast(data.message, 'error');
            console.log('error update');
          }
        },
        (err) => {
          this.presentToast(err, 'error');
          console.log('error', err);
        }
      );
  }

  GoBackToList() {
    this.router.navigate(['/institutes/institues-list']);
  }

  async presentToast(msg: any, messagetype) {
    const toast = await this.toastController.create({
      message: msg,
      duration: 3000,
      position: 'bottom',
      color: messagetype === 'success' ? 'success' : 'danger',
    });
    toast.present();
  }
}
