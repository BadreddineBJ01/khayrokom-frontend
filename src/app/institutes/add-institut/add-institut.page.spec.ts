import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { AddInstitutPage } from './add-institut.page';

describe('AddInstitutPage', () => {
  let component: AddInstitutPage;
  let fixture: ComponentFixture<AddInstitutPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddInstitutPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(AddInstitutPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
