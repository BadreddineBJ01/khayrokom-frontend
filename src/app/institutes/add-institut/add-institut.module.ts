import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';
import { AddInstitutPageRoutingModule } from './add-institut-routing.module';
import { AddInstitutPage } from './add-institut.page';
import { SharedModuleModule } from 'src/app/shared-module/shared-module.module';

@NgModule({
  imports: [
    CommonModule,
    SharedModuleModule,
    IonicModule,
    AddInstitutPageRoutingModule
  ],
  declarations: [AddInstitutPage]
})

export class AddInstitutPageModule {}
