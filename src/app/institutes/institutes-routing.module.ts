import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'institues-list',
    pathMatch: 'full'
  },
  {
    path: 'institues-list',
    loadChildren: () =>
      import('./institues-list/institues-list.module').then(
        m => m.InstituesListPageModule
      )
  },
  {
    path: 'add-institut',
    loadChildren: () =>
      import('./add-institut/add-institut.module').then(
        m => m.AddInstitutPageModule
      )
  },
  {
    path: 'institut-details/:id',
    loadChildren: () =>
      import('./institut-details/institut-details.module').then(
        m => m.InstitutDetailsPageModule
      )
  },
  {
    path: 'edit-institut/:id',
    loadChildren: () =>
      import('./add-institut/add-institut.module').then(
        m => m.AddInstitutPageModule
      )
  },
  {
    path: '**',
    redirectTo: 'institues-list'
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class InstitutesPageRoutingModule {}
