import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ClassesPage } from './classes.page';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'classe-list',
    pathMatch: 'full'
  },
  {
    path: 'add-class/:id',
    loadChildren: () => import('./add-class/add-class.module').then( m => m.AddClassPageModule)
  },
  {
    path: 'classe-list/:id',
    loadChildren: () => import('./classe-list/classe-list.module').then( m => m.ClasseListPageModule)
  },
  {
    path: 'class-details/:id',
    loadChildren: () => import('./class-details/class-details.module').then( m => m.ClassDetailsPageModule)
  },
  {
    path: 'edit-class/:id',
    loadChildren: () => import('./add-class/add-class.module').then( m => m.AddClassPageModule)
  },
  {
    path: '**',
    redirectTo: 'classe-list'
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ClassesPageRoutingModule {}
