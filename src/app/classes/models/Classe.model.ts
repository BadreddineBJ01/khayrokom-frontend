import {sharedModel} from '../../sharedModels/sharedModel';

export interface Classe {
  Classnamename: string;
  institutId: string;
}

export class ClasseModel extends sharedModel {
  Classname: string;
  institutId: string;

  constructor(ClasseEntity?: any) {
    ClasseEntity = ClasseEntity || {};
    super(ClasseEntity);
    this.Classname = ClasseEntity.Classname || '';
    this.institutId = ClasseEntity.institutId || '';
  }
}