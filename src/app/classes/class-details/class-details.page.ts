import { Component, OnInit } from '@angular/core';
import { ClassesService } from 'src/app/services/classes.service';
import { ActivatedRoute } from '@angular/router';
import { Classe,ClasseModel } from '../models/Classe.model';



@Component({
  selector: 'app-class-details',
  templateUrl: './class-details.page.html',
  styleUrls: ['./class-details.page.scss'],
})

export class ClassDetailsPage implements OnInit {
  ClassId: any;
  classDetails: any;

  constructor(private activatedRoute: ActivatedRoute,
              private classesService: ClassesService) { }

  ngOnInit() {
    this.ClassId = this.activatedRoute.snapshot.paramMap.get('id');
    this.getClassById(this.ClassId);
  }

  getClassById(ClassId: any) {
    this.classesService.getClassById(ClassId).subscribe(data => {
     if (data.code === '0000') {
       console.log(data.content);
       this.classDetails = data.content[0];
     } else {
      console.log('error', data);
     }
    }, (err) => {
      console.log('error', err);
    });
   }

   // format details.

}
