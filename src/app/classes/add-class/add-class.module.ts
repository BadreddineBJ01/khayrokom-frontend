import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { AddClassPageRoutingModule } from './add-class-routing.module';

import { AddClassPage } from './add-class.page';
import { SharedModuleModule } from 'src/app/shared-module/shared-module.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    AddClassPageRoutingModule,
    SharedModuleModule
  ],
  declarations: [AddClassPage]
})
export class AddClassPageModule {}
