import { Component, OnInit } from '@angular/core';
import {
  FormGroup,
  FormBuilder,
  Validators,
  NgForm,
  Form,
} from '@angular/forms';
import { ClassesService } from 'src/app/services/classes.service';
import { Router, ActivatedRoute, NavigationEnd } from '@angular/router';
import { ClasseModel } from '../models/Classe.model';
import { Subscription } from 'rxjs';
import { switchMap, map, zip } from 'rxjs/operators';

@Component({
  selector: 'app-add-class',
  templateUrl: './add-class.page.html',
  styleUrls: ['./add-class.page.scss'],
})
export class AddClassPage implements OnInit {
  ClassForm: FormGroup;
  ClassToEdit = new ClasseModel();
  EditClassInfo = false;
  classId: any;
  InstitutId: any;
  routerSubscription: Subscription;

  error_messages = {
    Classname: [{ type: 'required', message: 'Class name is required.' }],
  };

  constructor(
    public formBuilder: FormBuilder,
    public router: Router,
    public classesService: ClassesService,
    private activatedRoute: ActivatedRoute
  ) {}

  ngOnInit() {
    this.initForm();

    this.router.events
      .pipe(
        switchMap((navigation) =>
          this.activatedRoute.params.pipe(
            map((params) => ({ navigation, params }))
          )
        )
      )
      .subscribe((result) => {
        if (result.navigation instanceof NavigationEnd) {
          const urlfragment = result.navigation.url.split('/')[2];
          if (
            urlfragment.toLowerCase().includes('add') &&
            result.params.id !== undefined &&
            result.params !== {}
          ) {
            this.InstitutId = result.params.id;
            this.EditClassInfo = false;
            this.initForm();
          } else if (
            urlfragment.toLowerCase().includes('edit') &&
            result.params.id !== undefined &&
            result.params !== {}
          ) {
            this.GetClassById(result.params.id);
            this.classId = result.params.id;
            this.EditClassInfo = true;
          } else {
            console.log('error id undefined');
          }
        }
      });
  }

  GetClassById(ClassId: any) {
    console.log('params --->', ClassId);
    this.classesService.getClassById(ClassId).subscribe(
      (data) => {
        if (data.code === '0000') {
          console.log('data content', data.content[0]);
          this.ClassToEdit = new ClasseModel(data.content[0]);
          console.log('this.ClassToEdit', this.ClassToEdit);
          this.InstitutId = this.ClassToEdit.institutId;
          this.initForm();
        } else {
          console.log('error', data);
        }
      },
      (err) => {
        console.log('error', err);
      }
    );
  }

  initForm() {
    this.ClassForm = this.formBuilder.group({
      Classname: [this.ClassToEdit.Classname, Validators.required],
    });
  }

  addClass(ClassForm: FormGroup) {
    const classToAdd = { Classname: '', institutId: '' };
    classToAdd.Classname = ClassForm.value.Classname;
    classToAdd.institutId = this.InstitutId;
    console.log('add class', classToAdd);

    this.classesService.addClass(classToAdd).subscribe(
      (data) => {
        if (data.code === '0000') {
          console.log('class added with success');
          this.GoBackToList();
        } else {
          console.log('error');
        }
      },
      (err) => {
        console.log('error', err);
      }
    );
  }

  UpdateClass(ClassForm: FormGroup) {
    console.log('ClassForm.value', ClassForm.value, this.classId);
    this.classesService.updateClass(this.classId, ClassForm.value).subscribe(
      (data) => {
        if (data.code === '0000') {
          console.log('class updated with success');
          this.GoBackToList();
        } else {
          console.log('error update');
        }
      },
      (err) => {
        console.log('error', err);
      }
    );
  }

  GoBackToList() {
    console.log('---------> InstitutId', this.InstitutId);
    this.router.navigate(['/classes/classe-list/', this.InstitutId]);
  }
}
