import { Component, OnInit } from '@angular/core';
import { ClassesService } from 'src/app/services/classes.service';
import { Router, ActivatedRoute, NavigationEnd } from '@angular/router';
import {
  AlertController,
  ModalController,
  LoadingController,
} from '@ionic/angular';
import { ClassDetailsPage } from '../class-details/class-details.page';
import { UserService } from 'src/app/services/users.service';

@Component({
  selector: 'app-classe-list',
  templateUrl: './classe-list.page.html',
  styleUrls: ['./classe-list.page.scss'],
})
export class ClasseListPage implements OnInit {
  classesList: any;
  InstitutId: any;
  connectedUserRole: any;
  constructor(
    private classesService: ClassesService,
    private router: Router,
    private userService: UserService,
    private alertCtrl: AlertController,
    public modalController: ModalController,
    private loadingController: LoadingController,
    private activatedRoute: ActivatedRoute
  ) {}

  ngOnInit() {
    this.activatedRoute.params.subscribe((params: any) => {
      if (params !== {} && params.id !== undefined) {
        this.InstitutId = params.id;
      } else {
      }
    });
    this.setUserRole();

    this.router.events.subscribe((res) => {
      if (res instanceof NavigationEnd) {
        this.getAllInstitutClassesById();
      }
    });
  }

  async setUserRole() {
    this.connectedUserRole = await this.userService.getUserRole();
  }

  goToAddClass() {
    this.router.navigate(['classes/add-class/', this.InstitutId]);
  }

  async getAllInstitutClassesById() {
    const loading = await this.loadingController.create({
      message: 'Loading...',
    });
    await loading.present();

    this.classesService.getAllInstitutClassesById(this.InstitutId).subscribe(
      (data) => {
        if (data.code === '0000') {
          this.classesList = data.content;
          loading.dismiss();
        } else {
          loading.dismiss();
        }
      },
      (err) => {
        loading.dismiss();
      }
    );
  }

  editClasses(classId: any) {
    this.router.navigate(['classes/edit-class/' + classId]);
  }

  delete(classId: any) {
    this.classesService.deleteClass(classId).subscribe(
      (data) => {
        if (data.code === '0000') {
        } else {
          console.log('error');
        }
      },
      (err) => {
        console.log('error');
      }
    );
  }

  Classesdetails(classId: any) {
    this.router.navigate(['classes/class-details/' + classId]);
  }

  async presentConfirmDelete(classId: any) {
    const alert = this.alertCtrl.create({
      message: 'Do you want to delete this classe',
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          handler: () => {
            console.log('Cancel clicked');
          },
        },
        {
          text: 'Confirm',
          handler: () => {
            this.delete(classId);
            this.ReloadList();
          },
        },
      ],
    });

    (await alert).present();
  }
  ReloadList() {
    this.getAllInstitutClassesById();
  }
}
