import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { ClasseListPage } from './classe-list.page';

describe('ClasseListPage', () => {
  let component: ClasseListPage;
  let fixture: ComponentFixture<ClasseListPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ClasseListPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(ClasseListPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
