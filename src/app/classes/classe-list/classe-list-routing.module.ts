import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ClasseListPage } from './classe-list.page';

const routes: Routes = [
  {
    path: '',
    component: ClasseListPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ClasseListPageRoutingModule {}
