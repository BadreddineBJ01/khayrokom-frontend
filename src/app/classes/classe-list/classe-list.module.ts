import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ClasseListPageRoutingModule } from './classe-list-routing.module';

import { ClasseListPage } from './classe-list.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ClasseListPageRoutingModule
  ],
  declarations: [ClasseListPage]
})
export class ClasseListPageModule {}
