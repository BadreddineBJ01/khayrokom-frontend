export class sharedModel {
    _id: string;
    created_at: Date;

    constructor(entity: any) {
        this._id = entity._id;
        this.created_at = entity.created_at;
    }
}
