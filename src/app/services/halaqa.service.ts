import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from '../../environments/environment.dev';

@Injectable({
  providedIn: 'root'
})
export class HalaqaService {
  halaqaURL = environment.api + 'halaqa';
  InstitutHalaqatURL = environment.api + 'halaqats';


  constructor(private http: HttpClient) {}

  // endPoints
  getAllHalaqat(): Observable<any> {
    return this.http.get(this.halaqaURL);
  }

  getAllCheickHalaqats(InstitutId: any, CheickID: any): Observable<any> {
    return this.http.get(this.InstitutHalaqatURL + '/' + InstitutId + '/' + CheickID);
  }

  addHalaqa(Halaqa: any): Observable<any> {
    return this.http.post(this.halaqaURL, Halaqa);
  }

  getHalaqaById(halaqaId: any): Observable<any> {
    return this.http.get(this.halaqaURL + '/' + halaqaId);
  }

  updateHalaqa(halaqaId: any, HalaqaObject): Observable<any> {
    return this.http.put(this.halaqaURL + '/' + halaqaId, HalaqaObject);
  }

  deleteHalaqa(halaqaId: any): Observable<any> {
    return this.http.delete(this.halaqaURL + '/' + halaqaId);
  }
}
