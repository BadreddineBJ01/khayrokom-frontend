import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from '../../environments/environment.dev';

@Injectable({
  providedIn: 'root'
})
export class AchivementService {
  AchivementURL = environment.api + 'achivement';
  StudentAchivements = environment.api + 'achivement/student/';


  constructor(private http: HttpClient) {}

  // endPoints
  getAllStudentAchivements(StudentId: any): Observable<any> {
    return this.http.get(this.StudentAchivements + StudentId);
  }

  addAchivement(Achivement: any): Observable<any> {
    return this.http.post(this.AchivementURL, Achivement);
  }

  getAchivementById(AchivementId: any): Observable<any> {
    return this.http.get(this.AchivementURL + '/' + AchivementId);
  }

  updateAchivement(AchivementId: any, AchivementToUpdate): Observable<any> {
    return this.http.put(this.AchivementURL + '/' + AchivementId, AchivementToUpdate);
  }

  deleteAchivement(AchivementId: any): Observable<any> {
    return this.http.delete(this.AchivementURL + '/' + AchivementId);
  }
}