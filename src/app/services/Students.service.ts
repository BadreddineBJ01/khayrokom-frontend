import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from '../../environments/environment.dev';

@Injectable({
  providedIn: 'root'
})
export class StudentsService {
  StudentsURL = environment.api + 'student';
  HalaqaStudentsURL = environment.api + 'students/halaqa/';

  constructor(private http: HttpClient) {}

  // endPoints
  getAllHalqaStudents(halaqaId: any): Observable<any> {
    return this.http.get(this.HalaqaStudentsURL + halaqaId);
  }

  addStudent(Student: any): Observable<any> {
    return this.http.post(this.StudentsURL, Student);
  }

  getStudentById(StudentId: any): Observable<any> {
    return this.http.get(this.StudentsURL + '/' + StudentId);
  }

  updateStudent(StudentId: any, StudentToUpdate): Observable<any> {
    return this.http.put(this.StudentsURL + '/' + StudentId, StudentToUpdate);
  }

  deleteStudent(StudentId: any): Observable<any> {
    return this.http.delete(this.StudentsURL + '/' + StudentId);
  }
}