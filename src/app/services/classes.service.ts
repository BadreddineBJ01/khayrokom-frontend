import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from '../../environments/environment.dev';

@Injectable({
  providedIn: 'root'
})
export class ClassesService {
  ClassesURL = environment.api + 'class';
  InstitutClassesURL = environment.api + 'classes/institut/';

  constructor(private http: HttpClient) {}

  // endPoints

  getAllClasses(): Observable<any> {
    return this.http.get(this.ClassesURL);
  }

  getAllInstitutClassesById(InstitutId: any) : Observable<any>{
    return this.http.get(this.InstitutClassesURL + InstitutId);
  }

  addClass(classToSend: any): Observable<any> {
    return this.http.post(this.ClassesURL, classToSend);
  }

  getClassById(ClassId: any): Observable<any> {
    return this.http.get(this.ClassesURL + '/' + ClassId);
  }

  updateClass(ClassId: any, ClassToUpdate): Observable<any> {
    return this.http.put(this.ClassesURL + '/' + ClassId, ClassToUpdate);
  }

  deleteClass(ClassId: any): Observable<any> {
    return this.http.delete(this.ClassesURL + '/' + ClassId);
  }
}