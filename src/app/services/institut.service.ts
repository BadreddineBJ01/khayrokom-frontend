import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from '../../environments/environment.dev';
import { Institut } from '../institutes/models/Institut.model';

@Injectable({
  providedIn: 'root'
})
export class InstitutService {
  apiURL = environment.api;
  // backend urls
  InstitueURL = 'institute';

  constructor(private http: HttpClient) {}

  // institutes endpoints
  addInstitut(institut: Institut): Observable<any> {
    return this.http.post(this.apiURL + this.InstitueURL, institut);
  }

  getAllInstitutes(): Observable<any> {
    return this.http.get(this.apiURL + this.InstitueURL);
  }

  getInstitutById(institutId: any): Observable<any> {
    return this.http.get(this.apiURL + this.InstitueURL + '/' + institutId);
  }

  UpdateInstitut(institutId: any, institut: Institut): Observable<any> {
    return this.http.put(this.apiURL + this.InstitueURL + '/' + institutId, institut);
  }

  deleteInstitut(institutId: any): Observable<any> {
    return this.http.delete(this.apiURL + this.InstitueURL + '/' + institutId);
  }
}
