import { TestBed } from '@angular/core/testing';

import { InstitutService } from './institut.service';

describe('InstitutService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: InstitutService = TestBed.get(InstitutService);
    expect(service).toBeTruthy();
  });
});
