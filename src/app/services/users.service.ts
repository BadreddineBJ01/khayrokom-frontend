import { Injectable } from '@angular/core';
import { HttpClient, HttpClientJsonpModule } from '@angular/common/http';
import { Observable, BehaviorSubject } from 'rxjs';
import { environment } from '../../environments/environment.dev';
import { tap, concatMap } from 'rxjs/operators';
import { Storage } from '@ionic/storage';
import { Router } from '@angular/router';
import { EventEmitter } from 'events';

@Injectable({
  providedIn: 'root',
})
export class UserService {
  UserURL = environment.api + 'users';
  authSubject = new BehaviorSubject(false);
  roleSubject = new BehaviorSubject<string>('');
  logoutSubject = new BehaviorSubject<boolean>(false);

  constructor(
    private http: HttpClient,
    private storage: Storage,
    private router: Router
  ) {}

  loginUser(UserCredentials: any): Observable<any> {
    return this.http.post(this.UserURL + '/login', UserCredentials);
  }

  registerUser(UserToAdd: any): Observable<any> {
    return this.http.post(this.UserURL + '/register', UserToAdd);
  }

  // endPoints
  getAllUsers(): Observable<any> {
    return this.http.get(this.UserURL);
  }

  addUser(User: any): Observable<any> {
    return this.http.post(this.UserURL, User);
  }

  getUserById(UserId: any): Observable<any> {
    return this.http.get(this.UserURL + '/' + UserId);
  }

  updateUser(UserId: any, UserToUpdate): Observable<any> {
    return this.http.put(this.UserURL + '/' + UserId, UserToUpdate);
  }

  deleteUser(UserId: any): Observable<any> {
    return this.http.delete(this.UserURL + '/' + UserId);
  }

  logout() {
    this.logoutSubject.next(true);
    this.storage.clear();
    this.router.navigateByUrl('/auth');
  }

  async IsloggedIn() {
    const token = await this.storage.get('token');
    console.log('token', token);
    return token !== null;
  }

  async getUserInfo() {
    const userInfo = await this.storage.get('USERINFO');
    console.log('user info', userInfo);
    return userInfo;
  }

  async getUserRole() {
    const Role = await this.storage.get('ROLE');
    console.log('user role', Role);
    return Role;
  }

  async getConnectedUserId() {
    const userId = await this.storage.get('USERID');
    return userId;
  }

  requestReset(body): Observable<any> {
    return this.http.post(`${this.UserURL}/req-reset-password`, body);
  }

  newPassword(body): Observable<any> {
    return this.http.post(`${this.UserURL}/new-password`, body);
  }

  ValidPasswordToken(body): Observable<any> {
    return this.http.post(`${this.UserURL}/valid-password-token`, body);
  }
}
