
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from '../../environments/environment.dev';

@Injectable({
  providedIn: 'root'
})
export class SearchService {
  SearchURL = environment.api + 'globalSearch';
  StudentFilter = environment.api + 'achivement/student/';
  // /student/class/:halaqaId/:classId
  StudentFilterByClass = environment.api + 'student/class/';


  constructor(private http: HttpClient) {}

  Search(SearchValue: any){
   return this.http.get(this.SearchURL + '?q='+ SearchValue);
  }

  StudentProgramFilter(studentId: any, program: any){
    return this.http.get(this.StudentFilter + studentId + '/filter?program=' + program);
  }

  StudentByClass(halaqaId: any, classId: any) { 
  return this.http.get(this.StudentFilterByClass + halaqaId + '/' + classId);
  }




}
