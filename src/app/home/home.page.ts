import { Component } from '@angular/core';
import { MenuController } from '@ionic/angular';
import { SearchService } from '../services/search.service';
import { Router, NavigationEnd } from '@angular/router';
import { CommunicationService } from '../services/communication.service';
import { UserService } from '../services/users.service';
import { Storage } from '@ionic/storage';
import { countries } from '../institutes/models/Countries';
import { InstitutModel } from '../institutes/models/Institut.model';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {
  HalqaList: any[] = [];
  InstitutesList: any[] = [];
  ClassesList: any[] = [];
  StudentsList: any[] = [];
  nodataFound = true;
  countryArray = countries;

  constructor(
    private menuCtrl: MenuController,
    private communicationService: CommunicationService,
    private SearchService: SearchService,
    private storage: Storage,
    private userService: UserService,
    private router: Router
  ) {
    this.menuCtrl.enable(true, 'first');
  }

  ngOnInit() {
    this.getDataStorage();
  }

  onSearch(event: any) {
    console.log('event', event.target.value);
    if (event.target.value === '') {
      this.HalqaList.length = 0;
      this.InstitutesList.length = 0;
      this.ClassesList.length = 0;
      this.StudentsList.length = 0;
      this.nodataFound = true;
    } else {
      this.nodataFound = false;
      this.SearchService.Search(event.target.value).subscribe((data) => {
        if (data[0]) {
          this.HalqaList = data[0];
        }
        if (data[1]) {
          this.InstitutesList = data[1];
        }
        if (data[2]) {
          this.ClassesList = data[2];
        }
        if (data[3]) {
          this.StudentsList = data[3];
        }
      });
    }
  }

  getinstitutName(institut: InstitutModel) {
    let country = countries.find((el) => el.id.toString() == institut.country);
    return country ? country.name : '';
  }

  getDataStorage() {
    this.storage.get('ACCESSTOKEN').then((data: any) => {});
  }

  GoToHalqa(halaqaId: any) {
    this.router.navigate(['/students/students-list/' + halaqaId]);
  }

  GoToInstitut(institutId: any) {
    this.router.navigate(['/institutes/institut-details/' + institutId]);
  }

  GoToClass(classID: any) {
    this.router.navigate(['/classes/class-details/' + classID]);
  }

  GoToStudent(studentID: any) {
    this.router.navigate(['/achievements/achivement-list/' + studentID]);
  }

  async InstitutRedirection(institutId: any) {
    this.communicationService.InstitutId = institutId;
    let userRole = await this.userService.getUserRole();
    switch (userRole) {
      case 'admin': {
        this.router.navigate(['classes/classe-list/' + institutId]);
        break;
      }
      case 'cheick': {
        this.router.navigate(['/halaqat/halaqat-list/' + institutId]);
        break;
      }
      default: {
        console.log('user role is not setted');
      }
    }
  }
}
