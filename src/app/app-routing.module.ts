import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'auth',
    pathMatch: 'full',
  },
  {
    path: 'home',
    loadChildren: () =>
      import('./home/home.module').then((m) => m.HomePageModule),
  },
  {
    path: 'auth',
    loadChildren: () =>
      import('./auth/auth.module').then((m) => m.AuthPageModule),
  },
  {
    path: 'institutes',
    loadChildren: () =>
      import('./institutes/institutes.module').then(
        (m) => m.InstitutesPageModule
      ),
  },
  {
    path: 'halaqat',
    loadChildren: () =>
      import('./halaqat/halaqat.module').then((m) => m.HalaqatPageModule),
  },
  {
    path: 'classes',
    loadChildren: () =>
      import('./classes/classes.module').then((m) => m.ClassesPageModule),
  },
  {
    path: 'students',
    loadChildren: () =>
      import('./students/students.module').then((m) => m.StudentsPageModule),
  },
  {
    path: 'achievements',
    loadChildren: () =>
      import('./achievements/achievements.module').then(
        (m) => m.AchievementsPageModule
      ),
  },
  {
    path: 'users',
    loadChildren: () =>
      import('./users/users.module').then((m) => m.UsersPageModule),
  },
  {
    path: 'profile',
    loadChildren: () =>
      import('./profile/profile.module').then((m) => m.ProfilePageModule),
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { useHash: true })],
  exports: [RouterModule],
})
export class AppRoutingModule {}
